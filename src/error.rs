#[derive(Debug, Clone)]
pub enum GhwNumError {
    ULEB128,
    SLEB128,
    LSLEB128,
}

#[derive(Debug, Clone)]
pub enum GhwFileReadError {}
