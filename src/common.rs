use std::rc::Rc;

use crate::sections::ghwtypes::GhwType;
use nom::bytes::complete::{take, take_while};
use nom::combinator::{fail, map};
use nom::error::{context, ContextError, FromExternalError, ParseError};
use nom::number::complete::{f64, le_u8};
use nom::number::Endianness;
use nom::sequence::pair;
use nom::IResult;
use num::PrimInt;

use crate::error::GhwNumError;

#[repr(u8)]
#[derive(Copy, Clone, Debug, Primitive)]
/// equivalent to c enum ghdl_rtik, which enumerates all possible basic types, that are
/// used in ghdl simulation at runtime
pub enum GhdlRtik {
    Top = 0,
    Library = 1,
    Package = 2,
    PackageBody = 3,
    Entity = 4,
    Architecture = 5,
    Process = 6,
    Block = 7,
    IfGenerate = 8,
    ForGenerate = 9,
    Instance = 10,
    Constant = 11,
    Iterator = 12,
    Variable = 13,
    Signal = 14,
    File = 15,
    Port = 16,
    Generic = 17,
    Alias = 18,
    Guard = 19,
    Component = 20,
    Attribute = 21,
    TypeB2 = 22,
    TypeE8 = 23,
    TypeE32 = 24,
    TypeI32 = 25,
    TypeI64 = 26,
    TypeF64 = 27,
    TypeP32 = 28,
    TypeP64 = 29,
    TypeAccess = 30,
    TypeArray = 31,
    TypeRecord = 32,
    TypeFile = 33,
    SubtypeScalar = 34,
    SubtypeArray = 35,
    SubtypeArrayPtr = 36,
    SubtypeUnboundedArray = 37,
    SubtypeRecord = 38,
    SubtypeUnboundedRecord = 39,
}

#[repr(u8)]
#[derive(Copy, Debug, Clone, PartialEq, Primitive)]
/// equivalent to ghw_hie_kind enum in C implementation. Enumerates all possible types
/// of hierarchy items of the design
pub enum GhwHieKind {
    /// end of hierarchy
    Eoh = 0,
    /// top level block
    Design = 1,
    Block = 3,
    GenerateIf = 4,
    GenerateFor = 5,
    Instance = 6,
    Package = 7,
    Process = 13,
    Generic = 14,
    /// end of scope
    Eos = 15,
    Signal = 16,
    PortIn = 17,
    PortOut = 18,
    PortInout = 19,
    PortBuffer = 20,
    PortLinkage = 21,
}

#[derive(Debug, Clone, Copy)]
pub enum GhwVal {
    B2(u8),
    E8(u8),
    I32(i32),
    I64(i64),
    F64(f64),
}

/// A non-composite signal
#[derive(Debug, Clone)]
pub struct GhwSig {
    pub ghwtype: Rc<GhwType>,
    pub ghwval: GhwVal,
}

pub trait RtiKind {
    /// returns the kind of the item itself
    fn rti_kind(&self) -> GhdlRtik;
}

impl nom::error::FromExternalError<&[u8], GhwNumError> for GhwNumError {
    fn from_external_error(_input: &[u8], _kind: nom::error::ErrorKind, e: GhwNumError) -> Self {
        e
    }
}

/// parses unsigned ints, compressed to little endian and modulo 128, so that each msb
/// of each byte indicates, whether the next byte still belongs to this integer.
/// Compressed data are little endian format
pub fn uleb128_to_u<'a, T: PrimInt + num::cast::FromPrimitive, E>(
    i: &'a [u8],
) -> IResult<&'a [u8], T, E>
where
    E: ParseError<&'a [u8]>
        + std::fmt::Debug
        + ContextError<&'a [u8]>
        + nom::error::FromExternalError<&'a [u8], GhwNumError>,
{
    context(
        "uleb128_to_u",
        nom::combinator::map_res(
            pair(take_while(|c: u8| c & 0x80 == 0x80), take(1u8)),
            |c: (&[u8], &[u8])| {
                let mut bit_idx = 0;
                let mut number: u64 = 0;
                for bits7 in c.0 {
                    number |= ((bits7 & 127) as u64) << bit_idx;
                    bit_idx += 7;
                }
                number |= ((c.1[0] & 127) as u64) << bit_idx;
                // compile_warning!("find some non panic solution");
                match num::cast::FromPrimitive::from_u64(number) as Option<T> {
                    Some(e) => Ok(e),
                    None => Err(GhwNumError::ULEB128),
                }
            },
        ),
    )(i)
}

// todo: Proper errror type
pub fn ghwendianness(i: u8) -> Result<nom::number::Endianness, GhwNumError> {
    match i {
        2 => Ok(nom::number::Endianness::Big),
        1 => Ok(nom::number::Endianness::Little),
        _ => {
            println!("unknown endianness: {}", i);
            Err(GhwNumError::ULEB128)
        }
    }
}

/// decodes compressed i32 with modulo 128 and msb indicating whether next byte still
/// belongs to this integer. Compressed data are little endian format
pub fn sleb128_to_i32<'a, E>(i: &'a [u8]) -> IResult<&'a [u8], i32, E>
where
    E: ParseError<&'a [u8]> + std::fmt::Debug,
{
    map(
        pair(take_while(|c| c & 0x80 != 0), take(1u8)),
        |c: (&[u8], &[u8])| -> i32 {
            let mut bit_idx = 0;
            let mut number: i32 = 0;
            for bits in c.0 {
                number |= ((bits & 0x7f) as i32) << bit_idx;
                bit_idx += 7;
            }
            number |= ((c.1[0] & 0x7f) as i32) << bit_idx;
            bit_idx += 7;
            if c.1[0] & 0x40 != 0 && bit_idx < 32 {
                number |= !0i32 << bit_idx;
            }
            number
        },
    )(i)
}

/// decodes compressed i64 with modulo 128 and msb indicating whether next byte still
/// belongs to this integer. Compressed data are little endian format
pub fn lsleb128_to_i64<'a, E>(i: &'a [u8]) -> IResult<&'a [u8], i64, E>
where
    E: ParseError<&'a [u8]> + std::fmt::Debug,
{
    map(
        pair(take_while(|c| c & 0x80 != 0), take(1u8)),
        |c: (&[u8], &[u8])| -> i64 {
            let mut bit_idx = 0;
            let mut number: i64 = 0;
            for bits in c.0 {
                number |= ((bits & 0x7f) as i64) << bit_idx;
                bit_idx += 7;
            }
            number |= ((c.1[0] & 0x7f) as i64) << bit_idx;
            bit_idx += 7;
            if c.1[0] & 0x40 != 0 && bit_idx < 32 {
                number |= !0i64 << bit_idx;
            }
            number
        },
    )(i)
}

pub(crate) fn is_compressed_string_byte(b: u8) -> bool {
    b & 0b0110_0000 != 0
}

pub(crate) fn decode_strs_encoded_len(i: &[u8]) -> usize {
    let mut len = 0;
    for (byte_idx, _) in i.iter().enumerate() {
        // todo: warn if bit 8 is not set before the last element or bit 8 is set at last element
        len |= (i[byte_idx] as usize & 0x1f) << (byte_idx * 5);
    }
    len
}

impl GhwVal {
    pub fn parse<'a, E>(
        i: &'a [u8],
        ghw_type: Rc<GhwType>,
        endian: Endianness,
    ) -> IResult<&'a [u8], Self, E>
    where
        E: ParseError<&'a [u8]>
            + ContextError<&'a [u8]>
            + std::fmt::Debug
            + FromExternalError<&'a [u8], GhwNumError>,
    {
        let base_type = GhwType::base_type(&ghw_type);

        match &base_type {
            Some(t) => match t.rti_kind() {
                GhdlRtik::TypeB2 => {
                    IResult::Ok(context("GhwVal: TypeB2", map(le_u8, Self::B2))(i)?)
                }
                GhdlRtik::TypeE8 => {
                    IResult::Ok(context("GhwVal: TypeE8", map(le_u8, Self::E8))(i)?)
                }
                GhdlRtik::TypeI32 | GhdlRtik::TypeP32 => IResult::Ok(context(
                    "GhwVal: TypeI/P32",
                    map(sleb128_to_i32, Self::I32),
                )(i)?),
                GhdlRtik::TypeI64 | GhdlRtik::TypeP64 => IResult::Ok(context(
                    "GhwVal: TypeI/P64",
                    map(lsleb128_to_i64, Self::I64),
                )(i)?),
                GhdlRtik::TypeF64 => {
                    IResult::Ok(context("GhwVal: TypeF64", map(f64(endian), Self::F64))(i)?)
                }
                _ => {
                    println!("ERROR: type {:?} not supported", base_type);
                    context("GhwVal unsupported base GhwType", fail)(i)
                }
            },
            None => {
                println!("ERROR: type {:?} has no base type", ghw_type);
                context("GhwVal: GhwType has no base type", fail)(i)
            }
        }
    }
}
