#[cfg(test)]
mod common {
    use nom::number::Endianness;

    fn generate_lsleb128(n: i64) -> Vec<u8> {
        let mut buffer = Vec::new();
        let mut v: i64 = n as i64;
        let mut r: u8;
        loop {
            r = (v.rem_euclid(128)) as u8;
            v = v >> 7;
            println!("v: {:x}, r: {:x}", v, r);
            if (v == 0 && (r & 0x40) == 0) || (v == -1 && (r & 0x40) != 0) {
                buffer.push(r);
                break;
            } else {
                buffer.push(r | 0x80);
            }
        }
        println!("buffer: {:?}", buffer);
        buffer
    }

    mod uleb128 {
        use crate::common::uleb128_to_u;
        use nom::{
            error::{Error, VerboseError},
            IResult,
        };

        fn generate_e32_testvals(val: u32) -> Vec<u8> {
            let mut bytes = Vec::new();
            let mut v: u32 = val;
            let mut r: u32;
            loop {
                r = v % 128;
                v = v / 128;
                if v == 0 {
                    bytes.push(r as u8);
                    break;
                } else {
                    bytes.push((r + 128) as u8)
                }
            }
            bytes
        }
        #[test]
        fn e32_zero() {
            let testdata = [0u8];
            let parse_res: IResult<_, _, nom::error::VerboseError<_>> =
                uleb128_to_u::<u32, _>(&testdata);
            let parse_res = parse_res.unwrap();
            assert_eq!(0, parse_res.1, "wrong parse result");
            let empty_u8: &[u8] = &[];
            assert_eq!(empty_u8, parse_res.0);
        }
        #[test]
        fn e32_zero_2() {
            let testdata = [0u8, 0, 0];
            let parse_res = uleb128_to_u::<u32, nom::error::VerboseError<_>>(&testdata).unwrap();
            println!("------------------result: {:?}", parse_res);
            assert_eq!(0, parse_res.1);
            let two_u8: &[u8] = &[0, 0];
            assert_eq!(two_u8, parse_res.0);
        }
        #[test]
        fn e32_7bits() {
            let testvals = [1, 2, 3, 48, 49, 50, 63, 64, 126, 127];
            for val in testvals {
                let testdata = generate_e32_testvals(val);
                let parse_res =
                    uleb128_to_u::<u32, nom::error::VerboseError<_>>(&testdata).unwrap();
                assert_eq!(val, parse_res.1);
                let empty_u8: &[u8] = &[];
                assert_eq!(empty_u8, parse_res.0);
            }
        }
        #[test]
        fn e32_14bits() {
            let testvals = [128, 129, 130, 250, 255, 256, 16382, 16383];
            for val in testvals {
                let testdata = generate_e32_testvals(val);
                let parse_res =
                    uleb128_to_u::<u32, nom::error::VerboseError<_>>(&testdata).unwrap();
                assert_eq!(val, parse_res.1);
                let empty_u8: &[u8] = &[];
                assert_eq!(empty_u8, parse_res.0);
            }
        }
        #[test]
        fn e32_21bits() {
            let testvals = [16384, 16385, 65535, 65536, 2097150, 2097151];
            for val in testvals {
                let testdata = generate_e32_testvals(val);
                let parse_res =
                    uleb128_to_u::<u32, nom::error::VerboseError<_>>(&testdata).unwrap();
                assert_eq!(val, parse_res.1);
                let empty_u8: &[u8] = &[];
                assert_eq!(empty_u8, parse_res.0);
            }
        }
        #[test]
        fn e32_32bits() {
            let testvals = [2097152, 0b0001000_0000100_0000010_0000001, u32::MAX];
            for val in testvals {
                let testdata = generate_e32_testvals(val);
                let parse_res =
                    uleb128_to_u::<u32, nom::error::VerboseError<_>>(&testdata).unwrap();
                assert_eq!(val, parse_res.1);
                let empty_u8: &[u8] = &[];
                assert_eq!(empty_u8, parse_res.0);
            }
        }
        #[test]
        #[should_panic(expected = "overflow detected")]
        fn e32_overflow() {
            let testdata = [0xff, 0xff, 0xff, 0xff, 0xff, 0x01];
            let _res = uleb128_to_u::<u32, nom::error::VerboseError<_>>(&testdata)
                .expect("overflow detected");
        }
    }
    mod sleb128 {
        use crate::common::sleb128_to_i32;

        fn generate_sleb128(n: i32) -> Vec<u8> {
            let mut buffer = Vec::new();
            let mut v: i64 = n as i64;
            let mut r: u8;
            loop {
                r = (v.rem_euclid(128)) as u8;
                v = v >> 7;
                println!("v: {:x}, r: {:x}", v, r);
                if (v == 0 && (r & 0x40) == 0) || (v == -1 && (r & 0x40) != 0) {
                    buffer.push(r);
                    break;
                } else {
                    buffer.push(r | 0x80);
                }
            }
            println!("buffer: {:?}", buffer);
            buffer
        }

        #[test]
        fn sleb128_zero() {
            let testdata = generate_sleb128(0);
            println!("testdata: {:?}", testdata);
            assert_eq!(testdata.len(), 1, "wrong test data vector length");
            let parse_res = sleb128_to_i32::<nom::error::VerboseError<_>>(&testdata).unwrap();
            assert_eq!(0, parse_res.1, "wrong parsed data");
            let empty_u8: &[u8] = &[];
            assert_eq!(empty_u8, parse_res.0, "wrong remaining input");
        }

        #[test]
        fn sleb128_6_bit_non_neg() {
            let testvals = [0, 1, 2, 62, 63];
            for val in testvals {
                let testdata = generate_sleb128(val);
                println!("testdata: {:?}", testdata);
                assert_eq!(testdata.len(), 1, "wrong test data vector length");
                let parse_res = sleb128_to_i32::<nom::error::VerboseError<_>>(&testdata).unwrap();
                println!("parse result: {:?}", parse_res);
                assert_eq!(val, parse_res.1, "wrong parsed data");
                let empty_u8: &[u8] = &[];
                assert_eq!(empty_u8, parse_res.0, "wrong remaining input");
            }
        }

        #[test]
        fn sleb128_6_bit_neg() {
            let testvals = [-64, -63, -62, -60, -50, -40, -30, -10, -2, -1];
            for val in testvals {
                let testdata = generate_sleb128(val);
                println!("testdata: {:?}", testdata);
                assert_eq!(testdata.len(), 1, "wrong test data vector length");
                let parse_res = sleb128_to_i32::<nom::error::VerboseError<_>>(&testdata);
                println!("parse res 1.: {:?}", parse_res);
                let parse_res = parse_res.unwrap();
                assert_eq!(val, parse_res.1, "wrong parsed data");
                let empty_u8: &[u8] = &[];
                assert_eq!(empty_u8, parse_res.0, "wrong remaining input");
            }
        }

        #[test]
        fn sleb128_13_bit_non_neg() {
            let testvals = [64, 65, 4095, 4096, 4000, 8000, 8191, 8191];
            for val in testvals {
                let testdata = generate_sleb128(val);
                println!("testdata: {:?}", testdata);
                assert_eq!(testdata.len(), 2, "wrong test data vector length");
                let parse_res = sleb128_to_i32::<nom::error::VerboseError<_>>(&testdata).unwrap();
                assert_eq!(val, parse_res.1, "wrong parsed data");
                let empty_u8: &[u8] = &[];
                assert_eq!(empty_u8, parse_res.0, "wrong remaining input");
            }
        }

        #[test]
        fn sleb128_13_bit_neg() {
            let testvals = [-8192, -8191, -4095, -4094, -2000, -65];
            for val in testvals {
                let testdata = generate_sleb128(val);
                println!("testdata: {:?}", testdata);
                assert_eq!(testdata.len(), 2, "wrong test data vector length");
                let parse_res = sleb128_to_i32::<nom::error::VerboseError<_>>(&testdata).unwrap();
                assert_eq!(val, parse_res.1, "wrong parsed data");
            }
        }

        #[test]
        fn min_boundaries() {
            let testvals = i32::MIN..i32::MIN + 10;
            for val in testvals {
                let testdata = generate_sleb128(val);
                println!("testdata: {:?}", testdata);
                let parse_res = sleb128_to_i32::<nom::error::VerboseError<_>>(&testdata).unwrap();
                assert_eq!(val, parse_res.1, "wrong parsed data");
            }
        }
        #[test]
        fn max_boundaries() {
            let testvals = i32::MAX - 10..i32::MAX;
            for val in testvals {
                let testdata = generate_sleb128(val);
                println!("testdata: {:?}", testdata);
                let parse_res = sleb128_to_i32::<nom::error::VerboseError<_>>(&testdata).unwrap();
                assert_eq!(val, parse_res.1, "wrong parsed data");
            }
            let testdata = generate_sleb128(i32::MAX);
            println!("testdata: {:?}", testdata);
            let parse_res = sleb128_to_i32::<nom::error::VerboseError<_>>(&testdata).unwrap();
            assert_eq!(i32::MAX, parse_res.1, "wrong parsed data");
        }
    }

    mod lsleb128 {
        use crate::{common::lsleb128_to_i64, tests::common::generate_lsleb128};

        #[test]
        fn lsleb128_zero() {
            let testdata = generate_lsleb128(0);
            println!("testdata: {:?}", testdata);
            assert_eq!(testdata.len(), 1, "wrong test data vector length");
            let parse_res = lsleb128_to_i64::<nom::error::VerboseError<_>>(&testdata).unwrap();
            assert_eq!(0, parse_res.1, "wrong parsed data");
            let empty_u8: &[u8] = &[];
            assert_eq!(empty_u8, parse_res.0, "wrong remaining input");
        }

        #[test]
        fn lsleb128_6_bit_non_neg() {
            let testvals = [0, 1, 2, 62, 63];
            for val in testvals {
                let testdata = generate_lsleb128(val);
                println!("testdata: {:?}", testdata);
                assert_eq!(testdata.len(), 1, "wrong test data vector length");
                let parse_res = lsleb128_to_i64::<nom::error::VerboseError<_>>(&testdata).unwrap();
                println!("parse result: {:?}", parse_res);
                assert_eq!(val, parse_res.1, "wrong parsed data");
                let empty_u8: &[u8] = &[];
                assert_eq!(empty_u8, parse_res.0, "wrong remaining input");
            }
        }

        #[test]
        fn sleb128_6_bit_neg() {
            let testvals = [-64, -63, -62, -2, -1];
            for val in testvals {
                let testdata = generate_lsleb128(val);
                println!("testdata: {:?}", testdata);
                assert_eq!(testdata.len(), 1, "wrong test data vector length");
                let parse_res = lsleb128_to_i64::<nom::error::VerboseError<_>>(&testdata);
                println!("parse res 1.: {:?}", parse_res);
                let parse_res = parse_res.unwrap();
                assert_eq!(val, parse_res.1, "wrong parsed data");
                let empty_u8: &[u8] = &[];
                assert_eq!(empty_u8, parse_res.0, "wrong remaining input");
            }
        }

        #[test]
        fn sleb128_13_bit_non_neg() {
            let testvals = [64, 65, 4095, 4096, 1000, 8191, 8191];
            for val in testvals {
                let testdata = generate_lsleb128(val);
                println!("testdata: {:?}", testdata);
                assert_eq!(testdata.len(), 2, "wrong test data vector length");
                let parse_res = lsleb128_to_i64::<nom::error::VerboseError<_>>(&testdata).unwrap();
                assert_eq!(val, parse_res.1, "wrong parsed data");
                let empty_u8: &[u8] = &[];
                assert_eq!(empty_u8, parse_res.0, "wrong remaining input");
            }
        }

        #[test]
        fn sleb128_13_bit_neg() {
            let testvals = [-8192, -8191, -4095, -4094, -2000, -65];
            for val in testvals {
                let testdata = generate_lsleb128(val);
                println!("testdata: {:?}", testdata);
                assert_eq!(testdata.len(), 2, "wrong test data vector length");
                let parse_res = lsleb128_to_i64::<nom::error::VerboseError<_>>(&testdata).unwrap();
                assert_eq!(val, parse_res.1, "wrong parsed data");
            }
        }

        #[test]
        fn min_boundaries() {
            let testvals = i64::MIN..i64::MIN + 10;
            for val in testvals {
                let testdata = generate_lsleb128(val);
                println!("testdata: {:?}", testdata);
                let parse_res = lsleb128_to_i64::<nom::error::VerboseError<_>>(&testdata).unwrap();
                assert_eq!(val, parse_res.1, "wrong parsed data");
            }
        }
        #[test]
        fn max_boundaries() {
            let testvals = i64::MAX - 10..i64::MAX;
            for val in testvals {
                let testdata = generate_lsleb128(val);
                println!("testdata: {:?}", testdata);
                let parse_res = lsleb128_to_i64::<nom::error::VerboseError<_>>(&testdata).unwrap();
                assert_eq!(val, parse_res.1, "wrong parsed data");
            }
            let testdata = generate_lsleb128(i64::MAX);
            println!("testdata: {:?}", testdata);
            let parse_res = lsleb128_to_i64::<nom::error::VerboseError<_>>(&testdata).unwrap();
            assert_eq!(i64::MAX, parse_res.1, "wrong parsed data");
        }
    }
    use crate::common::ghwendianness;
    #[test]
    fn littleendian() {
        let res = ghwendianness(1).unwrap();
        assert_eq!(res, Endianness::Little, "little endianness");
    }
    #[test]
    fn bigendian() {
        let res = ghwendianness(2).unwrap();
        assert_eq!(res, Endianness::Big, "big endianness");
    }
    #[test]
    fn failedendian_zero() {
        let res = ghwendianness(0);
        if let Ok(_) = res {
            assert!(false, "should fail");
        }
    }
    #[test]
    fn failedendian() {
        for i in 3..u8::MAX {
            let res = ghwendianness(i);
            if let Ok(_) = res {
                assert!(false, "should fail");
            }
        }
    }
    mod GhwVal {
        use std::rc::Rc;

        use crate::{
            common::{GhdlRtik, GhwVal},
            sections::ghwtypes::{
                GhwRange, GhwRangeType, GhwSubtypeScalar, GhwType, GhwTypeEnum, GhwTypePhysical,
                GhwTypeScalar, GhwWktType,
            },
        };
        use nom::number::Endianness;

        use super::generate_lsleb128;

        #[test]
        fn b2_pass() {
            for stim in [0, 1, 2, 255] {
                let data = &[stim];
                let t = GhwType::Enum(GhwTypeEnum {
                    kind: GhdlRtik::TypeB2,
                    name: Rc::new(String::from("t_b2")),
                    wkt: GhwWktType::Unknown.into(),
                    lits: Vec::new(),
                });
                let (_next, val) = GhwVal::parse::<nom::error::VerboseError<_>>(
                    data,
                    Rc::new(t),
                    Endianness::Little,
                )
                .unwrap();
                match val {
                    GhwVal::B2(v) => {
                        assert_eq!(v, stim);
                    }
                    _ => {
                        panic!("parsing B2 failed");
                    }
                };
            }
        }
        #[test]
        fn e8_pass() {
            for stim in [0, 1, 2, 255] {
                let data = &[stim];
                let t = GhwType::Enum(GhwTypeEnum {
                    kind: GhdlRtik::TypeE8,
                    name: Rc::new(String::from("t_e8")),
                    wkt: GhwWktType::Unknown.into(),
                    lits: Vec::new(),
                });
                let (_next, val) = GhwVal::parse::<nom::error::VerboseError<_>>(
                    data,
                    Rc::new(t),
                    Endianness::Little,
                )
                .unwrap();
                match val {
                    GhwVal::E8(v) => {
                        assert_eq!(v, stim);
                    }
                    _ => {
                        panic!("parsing E8 failed");
                    }
                };
            }
        }
        #[test]
        fn i32_pass() {
            for stim in [
                i32::MIN,
                -128,
                -127,
                -126,
                -2,
                -1,
                0,
                1,
                2,
                127,
                128,
                255,
                256,
                i32::MAX,
            ] {
                let data = generate_lsleb128(stim as i64);
                let bt = GhwType::Scalar(GhwTypeScalar {
                    kind: GhdlRtik::TypeI32,
                    name: Rc::new(String::from("p32_t")),
                });
                let t = GhwType::SubScalar(GhwSubtypeScalar {
                    kind: GhdlRtik::TypeP32,
                    name: Rc::new(String::from("t_p32")),
                    base: Rc::new(bt),
                    rng: GhwRange::I32(GhwRangeType::<i32> {
                        kind: GhdlRtik::SubtypeScalar,
                        dir: crate::sections::ghwtypes::GhwDir::To,
                        left: -10,
                        right: 20,
                    }),
                });
                let (_next, val) = GhwVal::parse::<nom::error::VerboseError<_>>(
                    &data,
                    Rc::new(t),
                    Endianness::Little,
                )
                .unwrap();
                match val {
                    GhwVal::I32(v) => {
                        assert_eq!(v, stim);
                    }
                    _ => {
                        panic!("paring I32 failed");
                    }
                };
            }
        }
        #[test]
        fn p32_pass() {
            for stim in [
                i32::MIN,
                -128,
                -127,
                -126,
                -2,
                -1,
                0,
                1,
                2,
                127,
                128,
                255,
                256,
                i32::MAX,
            ] {
                let data = generate_lsleb128(stim as i64);
                let bt = GhwType::Physical(GhwTypePhysical {
                    kind: GhdlRtik::TypeP32,
                    name: Rc::new(String::from("p32_t")),
                    units: Rc::new(Vec::new()),
                });
                let t = GhwType::SubScalar(GhwSubtypeScalar {
                    kind: GhdlRtik::TypeP32,
                    name: Rc::new(String::from("t_p32")),
                    base: Rc::new(bt),
                    rng: GhwRange::I32(GhwRangeType::<i32> {
                        kind: GhdlRtik::SubtypeScalar,
                        dir: crate::sections::ghwtypes::GhwDir::To,
                        left: -10,
                        right: 20,
                    }),
                });
                let (_next, val) = GhwVal::parse::<nom::error::VerboseError<_>>(
                    &data,
                    Rc::new(t),
                    Endianness::Little,
                )
                .unwrap();
                match val {
                    GhwVal::I32(v) => {
                        assert_eq!(v, stim);
                    }
                    _ => {
                        panic!("paring I32 failed");
                    }
                };
            }
        }
        #[test]
        fn i64_pass() {
            for stim in [
                i64::MIN,
                -128,
                -127,
                -126,
                -2,
                -1,
                0,
                1,
                2,
                127,
                128,
                255,
                256,
                i64::MAX,
            ] {
                let data = generate_lsleb128(stim as i64);
                let bt = GhwType::Scalar(GhwTypeScalar {
                    kind: GhdlRtik::TypeI64,
                    name: Rc::new(String::from("i64_t")),
                });
                let t = GhwType::SubScalar(GhwSubtypeScalar {
                    kind: GhdlRtik::TypeI64,
                    name: Rc::new(String::from("t_x64")),
                    base: Rc::new(bt),
                    rng: GhwRange::I64(GhwRangeType::<i64> {
                        kind: GhdlRtik::SubtypeScalar,
                        dir: crate::sections::ghwtypes::GhwDir::To,
                        left: -10,
                        right: 20,
                    }),
                });
                let (_next, val) = GhwVal::parse::<nom::error::VerboseError<_>>(
                    &data,
                    Rc::new(t),
                    Endianness::Little,
                )
                .unwrap();
                match val {
                    GhwVal::I64(v) => {
                        assert_eq!(v, stim);
                    }
                    _ => {
                        panic!("paring I32 failed");
                    }
                };
            }
        }
        use std::primitive::f64;
        #[test]
        fn f64_pass() {
            for stim in vec![-10.0, -5.5, -4.2, 0.0, 1.0, 1.1, 19.9, 20.0] {
                let v1: f64 = stim;
                let data = v1.to_le_bytes();
                let bt = GhwType::Scalar(GhwTypeScalar {
                    kind: GhdlRtik::TypeF64,
                    name: Rc::new(String::from("f64_t")),
                });
                let t = GhwType::SubScalar(GhwSubtypeScalar {
                    kind: GhdlRtik::SubtypeScalar,
                    name: Rc::new(String::from("t_f32")),
                    base: Rc::new(bt),
                    rng: GhwRange::F64(GhwRangeType::<f64> {
                        kind: GhdlRtik::TypeF64,
                        dir: crate::sections::ghwtypes::GhwDir::To,
                        left: -10.0,
                        right: 20.0,
                    }),
                });
                let (_next, val) = GhwVal::parse::<nom::error::VerboseError<_>>(
                    &data,
                    Rc::new(t),
                    Endianness::Little,
                )
                .unwrap();
                match val {
                    GhwVal::F64(v) => {
                        assert_eq!(v, stim);
                    }
                    _ => {
                        panic!("paring F64 failed");
                    }
                };
            }
        }
    }
}
