use std::fs::File;
use std::io::{prelude::*, SeekFrom};

use error::GhwFileReadError;
use nom::IResult;
use sections::{ghwhierarchy::GhwHierarchy, ghwstrings::Strings, ghwtypes::GhwTypes};

use crate::sections::ghwcycles::Cycles;
use crate::sections::ghwdirectory::{Directory, Tail};
use crate::sections::ghwfileheader::GhwFileHeader;
use crate::sections::ghwsnapshot::Snapshot;

use tracing::{debug, error, info, span, warn, Level};
use tracing_unwrap::{OptionExt, ResultExt};

#[macro_use]
extern crate enum_primitive_derive;
extern crate num_traits;

/// types and crates common to multiple modules
pub mod common;
/// error types
pub mod error;
/// types, functions and implementations for different sections of the GHW file
pub mod sections;
// private, only to trigger tests
mod tests;

/// represents the content of a GhwFile
#[derive(Debug)]
pub struct GhwFile {
    pub strings: Strings,
    pub types: GhwTypes,
    pub hierarchy: GhwHierarchy,
    pub cycles: Cycles,
}

impl GhwFile {
    /// tries to parse the file
    pub fn parse(p: &std::path::Path) -> Result<Self, GhwFileReadError> {
        let fn_span = span!(Level::TRACE, "parsing");
        let _enter = fn_span.enter();
        // init buffer with extra space
        let mut buf = vec![0u8; 64];
        let mut buf2;
        let mut f = File::open(p).unwrap_or_log();
        f.read_exact(&mut buf).unwrap_or_log();
        let file_header: IResult<_, _, nom::error::VerboseError<_>> = GhwFileHeader::parse(&buf);
        let file_header = file_header.unwrap_or_log().1;
        // get sections from directory
        buf.clear();
        f.seek(SeekFrom::End(-12)).unwrap_or_log();
        f.read_to_end(&mut buf).unwrap_or_log();
        let tail: IResult<_, _, nom::error::VerboseError<_>> = Tail::parse(&buf);
        let tail = tail.expect_or_log("tail parsing failed").1;
        buf.clear();
        f.seek(SeekFrom::Start(tail.dir_pos as u64)).unwrap_or_log();
        f.read_to_end(&mut buf).unwrap_or_log();
        let dir: IResult<_, _, nom::error::VerboseError<_>> = Directory::parse(&buf);
        let dir = dir.expect_or_log("dir parsing failed").1;
        // strings
        buf = vec![0; dir.strings.len as usize];
        f.seek(SeekFrom::Start(dir.strings.pos as u64))
            .unwrap_or_log();
        f.read_exact(&mut buf).unwrap_or_log();
        let strings: IResult<_, _, nom::error::VerboseError<_>> = Strings::parse(&buf);
        let strings = strings.expect_or_log("strings parsing failed").1;
        // types
        buf = vec![0; dir.types.len as usize];
        buf2 = vec![0; dir.types.extention.unwrap_or_log().1 as usize];
        f.seek(SeekFrom::Start(dir.types.pos as u64))
            .unwrap_or_log();
        f.read_exact(&mut buf).unwrap_or_log();
        f.seek(SeekFrom::Start(
            dir.types.extention.unwrap_or_log().0 as u64,
        ))
        .unwrap_or_log();
        f.read_exact(&mut buf2).unwrap_or_log();
        buf.append(&mut buf2);
        let types: IResult<_, _, nom::error::VerboseError<_>> =
            GhwTypes::parse(&buf, strings.strs.clone(), file_header.endian);
        let types = types.expect_or_log("types parsing failed").1;
        // hierarchy
        buf = vec![0; dir.hierarchy.len as usize];
        buf2 = vec![0; dir.hierarchy.extention.unwrap_or_log().1 as usize];
        f.seek(SeekFrom::Start(dir.hierarchy.pos as u64))
            .unwrap_or_log();
        f.read_exact(&mut buf).unwrap_or_log();
        f.seek(SeekFrom::Start(
            dir.hierarchy.extention.unwrap_or_log().0 as u64,
        ))
        .unwrap_or_log();
        f.read_exact(&mut buf2).unwrap_or_log();
        buf.append(&mut buf2);
        let hierarchy: IResult<_, _, nom::error::VerboseError<_>> = GhwHierarchy::parse(
            &buf,
            strings.strs.clone(),
            types.type_table.clone(),
            file_header.endian,
        );
        let mut hierarchy = hierarchy.expect_or_log("hierarchy parsing failed").1;
        // snapshot
        buf = vec![0; dir.snapshot.len as usize];
        f.seek(SeekFrom::Start(dir.snapshot.pos as u64))
            .unwrap_or_log();
        f.read_exact(&mut buf).unwrap_or_log();
        let snapshot: IResult<_, _, nom::error::VerboseError<_>> =
            Snapshot::parse(&buf, &mut hierarchy, file_header.endian);
        let snapshot = snapshot.expect_or_log("snapshot parsing failed").1;
        // cycles
        buf = vec![0; dir.cycles.len as usize];
        f.seek(SeekFrom::Start(dir.cycles.pos as u64))
            .unwrap_or_log();
        f.read_exact(&mut buf).unwrap_or_log();
        let cycles: IResult<_, _, nom::error::VerboseError<_>> =
            Cycles::parse_all(&buf, &mut hierarchy, snapshot, file_header.endian);
        let cycles = cycles.expect_or_log("cycles parsing failed").1;
        Ok(Self {
            strings,
            types,
            hierarchy,
            cycles,
        })
    }
}
