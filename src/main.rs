use std::path::Path;

use clap::Parser;
use libghw_rs::GhwFile;

use svg::node::element;
use svg::node::element::path::Data;
use svg::node::element::{Group, TSpan, Text};
use svg::Document;

use tracing;
use tracing::{debug, error, info, warn};
use tracing::{event, Level};
use tracing_subscriber::fmt;

#[derive(clap::Parser, Debug)]
#[command(author, version, about)]
struct Args {
    #[arg(short, long)]
    input_file: String,
    #[arg(short, long, default_value_t = 2000)]
    width: u32,
    #[arg(short, long, default_value_t=Level::WARN)]
    log_level: Level,
}

// returns unit prefix and scaling factor for base unit femto
fn si_unit_prefix(n: f32) -> (&'static str, f32) {
    if n > 1.0 {
        ("", 1f32)
    } else if n > 1E-3 {
        ("m", 1E3)
    } else if n > 1E-6 {
        ("µ", 1E6)
    } else if n > 1E-9 {
        ("n", 1E9)
    } else if n > 1E-12 {
        ("p", 1E12)
    } else {
        ("f", 1E15)
    }
}

fn start_signals(f: &GhwFile) -> Vec<Group> {
    let mut gv = Vec::new();
    // add time scale
    let mut tsc_g = Group::new();
    // calculate time step size
    let end_time = f.cycles.cycs.last().unwrap().0;
    // in seconds
    let step_size = (end_time as f32) / 20f32 * 1E-15;
    let mut new_step_size = 10f32.powf(step_size.log(10.0).round() as f32);
    if end_time as f32 * 1E-15 / new_step_size < 15.0 {
        new_step_size = 0.5 * new_step_size;
    }
    let (u, p) = si_unit_prefix(step_size);
    for step in 0..20 {
        tsc_g = tsc_g.add(
            Text::new()
                .set("x", 30 + step * 100)
                .set("y", 15)
                .add(TSpan::new().add(svg::node::Text::new(format!(
                    "{:.1}{}s",
                    step as f32 * new_step_size * p,
                    u
                )))),
        )
    }
    println!(
        "step size: {}fs, new_step_size: {}fs",
        step_size, new_step_size
    );
    println!("step size: {:.1}{}s", step_size * p, u);
    println!("time step base: {}s", u);
    gv.push(tsc_g);
    // labels
    for idx in 1..11 {
        let ts = format!("item {}", idx);
        let g = Group::new().add(
            Text::new()
                .set("x", 5)
                .set("y", 15 + idx * 30)
                .add(TSpan::new().add(svg::node::Text::new(ts))),
        );
        gv.push(g);
    }
    gv
}

fn write_signals(p: &Path, gv: Vec<Group>) {
    let mut doc = Document::new()
        .set("width", 2200)
        .set("height", 30 * gv.len());
    for g in gv {
        doc = doc.add(g);
    }
    svg::save(p, &doc).unwrap();
}

fn main() {
    let args = Args::parse();
    let sub = fmt()
        .with_level(true)
        .with_target(false)
        .with_thread_ids(false)
        .with_thread_names(false)
        .with_max_level(args.log_level)
        .finish();
    tracing::subscriber::set_global_default(sub).unwrap();
    info!("log level: {}", args.log_level);
    let ghw_file = GhwFile::parse(Path::new(&args.input_file)).unwrap();

    let dv = start_signals(&ghw_file);
    write_signals(Path::new("trace.svg"), dv);
}
