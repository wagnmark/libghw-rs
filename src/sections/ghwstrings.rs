use std::rc::Rc;

use nom::{
    bytes::complete::{tag, take, take_while},
    combinator::map,
    error::{context, ContextError, ParseError},
    multi::count,
    number::complete::le_i32,
    sequence::{pair, preceded, tuple},
    IResult,
};
use tracing::{info, span, trace, warn, Level};

use crate::common::{decode_strs_encoded_len, is_compressed_string_byte};

/// contains all strings used in the GHW file
#[derive(Debug, Clone)]
pub struct Strings {
    pub strs: Rc<Vec<Rc<String>>>,
}

impl Strings {
    /// parsing the strings needed for other sections from a byte stream
    pub fn parse<'a, E>(i: &'a [u8]) -> IResult<&[u8], Self, E>
    where
        E: ParseError<&'a [u8]> + ContextError<&'a [u8]>,
    {
        let fn_span = span!(Level::INFO, "strings");
        let _enter = fn_span.enter();
        let mut next = i;
        let mut strs = Vec::new();
        // compile_warning!("better error handling");
        // first u32 header string count, second header string byte length
        let (n, (nbr_strs, nbr_chars)) = context(
            "strings section header",
            preceded(pair(tag(b"STR"), take(5u8)), tuple((le_i32, le_i32))),
        )(next)?;
        next = n;
        let (n, raw_strs) = context(
            "strings section body",
            count(
                context(
                    "strings body string",
                    pair(
                        map(
                            take_while(|b| !is_compressed_string_byte(b)),
                            decode_strs_encoded_len,
                        ),
                        take_while(is_compressed_string_byte),
                    ),
                ),
                nbr_strs as usize,
            ),
        )(next)?;
        next = n;
        info!(
            "strings header anounces {} strings with {} bytes",
            nbr_strs, nbr_chars
        );
        let mut bytes = Vec::new();
        for item in raw_strs {
            bytes.truncate(item.0);
            let mut v = Vec::from(item.1);
            bytes.append(&mut v);
            let raw_str = String::from_utf8(bytes.clone()).unwrap();
            trace!("found str \"{}\"", raw_str);
            strs.push(Rc::new(raw_str));
        }
        if nbr_strs != strs.len() as i32 {
            warn!("expected {} strings, but got {}", nbr_strs, strs.len());
        }
        let (n, _end_marker) = context("end of strings marker", tag(b"\0EOS"))(next)?;
        next = n;
        if next.len() > 0 {
            warn!("remaining input: {:?}", next);
        }
        IResult::Ok((
            next,
            Strings {
                strs: Rc::new(strs),
            },
        ))
    }
}

#[doc(hidden)]
#[cfg(test)]
mod test {
    use nom::IResult;

    use crate::sections::ghwstrings::Strings;
    const STRS_EXAMPLE: &[u8] = &[
        0x53, 0x54, 0x52, 0x00, 0x00, 0x00, 0x00, 0x00, 0x17, 0x00, 0x00, 0x00, 0x72, 0x00, 0x00,
        0x00, 0x27, 0x2d, 0x27, 0x01, 0x30, 0x27, 0x01, 0x31, 0x27, 0x01, 0x48, 0x27, 0x01, 0x4c,
        0x27, 0x01, 0x55, 0x27, 0x01, 0x57, 0x27, 0x01, 0x58, 0x27, 0x01, 0x5a, 0x27, 0x00, 0x61,
        0x01, 0x73, 0x79, 0x6e, 0x63, 0x5f, 0x73, 0x69, 0x67, 0x00, 0x62, 0x00, 0x63, 0x01, 0x6c,
        0x6b, 0x00, 0x64, 0x00, 0x72, 0x75, 0x6e, 0x63, 0x6c, 0x6b, 0x00, 0x73, 0x74, 0x61, 0x6e,
        0x64, 0x61, 0x72, 0x64, 0x02, 0x64, 0x5f, 0x6c, 0x6f, 0x67, 0x69, 0x63, 0x09, 0x5f, 0x31,
        0x31, 0x36, 0x34, 0x04, 0x75, 0x6c, 0x6f, 0x67, 0x69, 0x63, 0x02, 0x69, 0x6d, 0x75, 0x6c,
        0x69, 0x01, 0x79, 0x6e, 0x63, 0x5f, 0x6f, 0x72, 0x00, 0x74, 0x62, 0x5f, 0x65, 0x78, 0x61,
        0x6d, 0x70, 0x6c, 0x65, 0x00, 0x45, 0x4f, 0x53, 0x12, 0x35,
    ];
    #[test]
    fn valid_1() {
        let strs_res: IResult<_, _, nom::error::VerboseError<_>> = Strings::parse(STRS_EXAMPLE);
        let strs = strs_res.unwrap();
        println!("parsed string-obj: {:?}", strs);
        assert_eq!(
            strs.0,
            [0x12, 0x35],
            "wrong elements left over (number or length or values)"
        );
        let should_strs = vec![
            "'-'",
            "'0'",
            "'1'",
            "'H'",
            "'L'",
            "'U'",
            "'W'",
            "'X'",
            "'Z'",
            "a",
            "async_sig",
            "b",
            "c",
            "clk",
            "d",
            "runclk",
            "standard",
            "std_logic",
            "std_logic_1164",
            "std_ulogic",
            "stimuli",
            "sync_or",
            "tb_example",
        ];
        let mut strs_len = 0;
        for str in &should_strs {
            strs_len += str.len();
        }
        println!("should str len: {}", strs_len);
        assert_eq!(strs.1.strs.len(), should_strs.len());
        for idx in 0..should_strs.len() {
            assert_eq!(
                strs.1.strs[idx].as_ref(),
                should_strs[idx],
                "strings do not match"
            );
        }
    }
}
