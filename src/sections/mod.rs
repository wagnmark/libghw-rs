/// section describing the states at each simulation iteration
pub mod ghwcycles;
/// section describing the content of the ghw file
pub mod ghwdirectory;
/// file header information and file type verification
pub mod ghwfileheader;
/// section describing the hierarchy of the simulation design
pub mod ghwhierarchy;
/// section describing the inital state of the simulation
pub mod ghwsnapshot;
/// section, containing all strings used in the file
pub mod ghwstrings;
/// section describing all types of the simulation design
pub mod ghwtypes;
