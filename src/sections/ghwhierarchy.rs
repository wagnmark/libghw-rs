use std::{cell::RefCell, fmt::Debug, ops::Deref, rc::Rc};

use nom::{
    bytes::complete::tag,
    combinator::{fail, map, map_opt, verify},
    error::{context, ContextError, FromExternalError, ParseError},
    number::complete::{i32, le_u8},
    number::Endianness,
    sequence::{pair, preceded, tuple},
    IResult,
};

use num_traits::FromPrimitive;
use tracing::{debug, error, span, trace, warn, Level};

use crate::{
    common::{uleb128_to_u, GhwHieKind, GhwVal},
    error::GhwNumError,
};

use super::ghwtypes::GhwType;

/// main element, that contains the root element of the hierarchy tree
#[derive(Debug, Clone)]
pub struct GhwHierarchy {
    /// number of signals in hierarchy (also composite)
    pub num_sigs: u32,
    /// number of scalar signals (can also be part of composite signal)
    pub num_scalar_sigs: u32,
    /// root element of the tree
    pub root: Rc<GhwHie>,
    // pointer to a signal values, only used as internal buffer
    signal_types: Option<Vec<Rc<GhwType>>>,
}

/// an element of the design hierarchy
#[derive(Clone)]
pub struct GhwHie {
    kind: GhwHieKind,
    parent: Option<Rc<GhwHie>>,
    name: Rc<String>,
    brother: RefCell<Option<Rc<GhwHie>>>,
    body: GhwHieBody,
}

/// helper to represent the body of the hierarchy element
#[derive(Debug, Clone)]
pub enum GhwHieBody {
    /// a block in the design, i.e. an instance, a process
    Block(GhwHieBlock),
    /// an signal like element, i.e. a signal or a port
    Signal(GhwHieSignal),
    /// Elements not directly part of hierarchy: EOS or EOH
    Empty,
}

/// helper to represent the body variant block of the hierarchy element
#[derive(Debug, Clone)]
pub struct GhwHieBlock {
    child: RefCell<Option<Rc<GhwHie>>>,
    iter_type: Option<Rc<GhwType>>,
    iter_value: Option<GhwVal>,
    // TODO
}

/// helper to represent the body variant of the hierarchy element
#[derive(Debug, Clone)]
pub struct GhwHieSignal {
    ghwtype: Rc<GhwType>,
    /// signal ids and types, last one in GHW file represented by zero
    sigs: Vec<(u32, Rc<GhwType>)>,
    // TODO
}

impl GhwHierarchy {
    /// parse the hierarchy from a byte stream
    /// # Arguments
    ///
    /// * `i` - input byte stream
    /// * `strs` - containing the ghw file list of strings, which all other sections refer to by id
    /// * `types` - containing the ghw file list of types, which all other sections refer to by id
    /// * `endian` - endianness of the file, obtained from file header section, tail for directory
    pub fn parse<'a, E>(
        i: &'a [u8],
        strs: Rc<Vec<Rc<String>>>,
        types: Rc<Vec<Rc<GhwType>>>,
        endian: Endianness,
    ) -> IResult<&'a [u8], Self, E>
    where
        E: ParseError<&'a [u8]>
            + ContextError<&'a [u8]>
            + std::fmt::Debug
            + FromExternalError<&'a [u8], GhwNumError>,
    {
        let fn_span = span!(Level::INFO, "hierarchy");
        let _enter = fn_span.enter();
        let (mut next, (_nbr_scopes, nbr_scope_signals, sig_table_len)) = context(
            "hierarchy, header",
            preceded(
                context("magic header", pair(tag(b"HIE\0"), tag(&[0, 0, 0, 0]))),
                context(
                    "length items",
                    tuple((i32(endian), i32(endian), i32(endian))),
                ),
            ),
        )(i)?;
        println!("sc sig: {}, sig tab: {}", nbr_scope_signals, sig_table_len);
        if nbr_scope_signals < 0 {
            return context("number of signals shout be >= 0", fail)(next);
        }
        if sig_table_len < 0 {
            return context("number of signals shout be >= 0", fail)(next);
        }
        let design = GhwHie {
            kind: GhwHieKind::Design,
            parent: None,
            name: Rc::new(String::from("top")),
            brother: RefCell::new(None),
            body: GhwHieBody::Block(GhwHieBlock {
                child: RefCell::new(None),
                iter_type: None,
                iter_value: None,
            }),
        };
        let mut parent_stack = Vec::new();
        let root = Rc::new(design);
        let mut needs_brother: Option<Rc<GhwHie>> = None;
        let mut needs_child: Option<Rc<GhwHie>> = Some(root.clone());
        parent_stack.push(root.clone());
        loop {
            let (n, hie) = GhwHie::parse(
                next,
                strs.clone(),
                types.clone(),
                parent_stack.last().unwrap().clone(),
                endian,
            )?;
            next = n;
            let inner_last = Rc::new(hie);
            trace!(
                "sorting element {} ({:?}) into the tree",
                inner_last.name,
                inner_last.kind
            );
            match inner_last.body.clone() {
                GhwHieBody::Block(_) => {
                    if inner_last.kind == GhwHieKind::Process {
                        match &needs_child {
                            Some(p) => {
                                match &p.body {
                                    GhwHieBody::Block(b) => {
                                        b.child.replace(Some(inner_last.clone()))
                                    }
                                    _ => todo!("Error: ownly block bodies allowed"),
                                };
                            }
                            None => {}
                        };
                        match &needs_brother {
                            Some(b) => {
                                b.brother.replace(Some(inner_last.clone()));
                            }
                            None => {}
                        }
                        needs_child = None;
                        needs_brother = Some(inner_last.clone())
                    } else {
                        match &needs_child {
                            Some(p) => {
                                match &p.body {
                                    GhwHieBody::Block(b) => {
                                        b.child.replace(Some(inner_last.clone()))
                                    }
                                    _ => todo!("Error: ownly block bodies allowed"),
                                };
                            }
                            None => {}
                        };
                        match &needs_brother {
                            Some(b) => {
                                b.brother.replace(Some(inner_last.clone()));
                            }
                            None => {}
                        }
                        needs_child = Some(inner_last.clone());
                        needs_brother = None;
                        parent_stack.push(inner_last.clone());
                    };
                }
                GhwHieBody::Signal(_) => {
                    match &needs_child {
                        Some(p) => {
                            match &p.body {
                                GhwHieBody::Block(b) => b.child.replace(Some(inner_last.clone())),
                                _ => todo!("Error: only block bodies allowed"),
                            };
                        }
                        None => {}
                    };
                    match &needs_brother {
                        Some(b) => {
                            b.brother.replace(Some(inner_last.clone()));
                        }
                        None => {}
                    }
                    needs_child = None;
                    needs_brother = Some(inner_last.clone())
                }
                GhwHieBody::Empty => match inner_last.kind {
                    GhwHieKind::Eos => {
                        match parent_stack.pop() {
                            Some(old_parent) => {
                                needs_brother = Some(old_parent);
                            }
                            _ => {
                                warn!("only end of scope, but ran out of parents");
                                break;
                            }
                        };
                        needs_child = None;
                    }
                    GhwHieKind::Eoh => {
                        {
                            debug!("end of hierarchy detected");
                            break;
                        };
                    }
                    _ => {
                        panic!("invalid end of scope ?header; kind: {:?}", inner_last.kind);
                    }
                },
            };
        }
        GhwHie::print_tree(root.clone(), 0, 5);
        if next.len() > 0 {
            warn!("remaining input: {:?}", next);
        }
        IResult::Ok((
            next,
            Self {
                root,
                num_sigs: nbr_scope_signals as u32,
                num_scalar_sigs: sig_table_len as u32,
                signal_types: None,
            },
        ))
    }

    /// get the type of the signal at index `idx` (zero index)
    pub fn get_signal_type(&mut self, idx: u32) -> Option<Rc<GhwType>> {
        if self.signal_types.is_none() && !self.try_build_signal_type_index() {
            return None;
        };
        match &self.signal_types {
            Some(sv) => {
                if idx as usize >= sv.len() {
                    None
                } else {
                    Some(sv[idx as usize].clone())
                }
            }
            None => None,
        }
    }

    /// get all signal types in ascending order
    pub fn get_signal_types(&mut self) -> Option<Vec<Rc<GhwType>>> {
        if self.signal_types.is_none() && !self.try_build_signal_type_index() {
            return None;
        };
        self.signal_types.clone()
    }

    fn try_build_signal_type_index(&mut self) -> bool {
        let fn_span = span!(Level::INFO, "sigidxfromtree");
        let _enter = fn_span.enter();
        let mut possible_new_table: Vec<Option<Rc<GhwType>>> =
            vec![None; self.num_scalar_sigs as usize];
        let mut item = self.root.clone();
        let mut parent_stack = vec![self.root.clone()];
        let mut stack_empty = false;
        loop {
            trace!(
                "name: {} (hierarchy level: {})",
                item.name,
                parent_stack.len()
            );
            if let GhwHieBody::Signal(s) = &item.body {
                for (idx, sig) in &s.sigs {
                    if *idx > self.num_scalar_sigs {
                        error!("signal index to big");
                        return false;
                    } else if *idx == 0 {
                        error!("signal index is zero");
                        return false;
                    } else if possible_new_table[*idx as usize - 1].is_none()
                        && GhwType::base_type(sig).is_some()
                    {
                        possible_new_table[*idx as usize - 1] = Some(sig.clone());
                    }
                }
            }
            if let Some(child) = item.child() {
                parent_stack.push(item);
                item = child;
            } else if let Some(b) = item.brother() {
                item = b;
            } else {
                loop {
                    match parent_stack.pop() {
                        Some(i) => match i.brother() {
                            Some(i) => {
                                item = i;
                                break;
                            }
                            _ => continue,
                        },
                        None => {
                            debug!("parent stack empty, returning");
                            stack_empty = true;
                            break;
                        }
                    };
                }
            }
            if stack_empty {
                break;
            }
        }
        let mut temp_vec = Vec::new();
        let mut has_signal = true;
        for (idx, item) in possible_new_table.iter().enumerate() {
            match &item {
                Some(t) => temp_vec.push(t.clone()),
                None => {
                    warn!("missing signal at index {}", idx);
                    has_signal = false;
                }
            };
        }
        if has_signal {
            self.signal_types = Some(temp_vec);
        }
        has_signal
    }
}

impl GhwHie {
    /// Parses a single hierarchy item from a byte stream. Brother must be set manually afterwards.
    /// # Arguments
    ///
    /// * `i` - input byte stream
    /// * `strs` - containing the ghw file list of strings, which all other sections refer to by id
    /// * `types` - containing the ghw file list of types, which all other sections refer to by id
    /// * `parent` - `GhwHie` item, that should be set as the parent of the new item
    /// * `endian` - endianness of the file, obtained from file header section, tail for directory
    pub fn parse<'a, E>(
        i: &'a [u8],
        strs: Rc<Vec<Rc<String>>>,
        types: Rc<Vec<Rc<GhwType>>>,
        parent: Rc<GhwHie>,
        endian: Endianness,
    ) -> IResult<&'a [u8], Self, E>
    where
        E: ParseError<&'a [u8]>
            + ContextError<&'a [u8]>
            + std::fmt::Debug
            + FromExternalError<&'a [u8], GhwNumError>,
    {
        let (mut next, kind) = context("GhwHie: get kind", map_opt(le_u8, GhwHieKind::from_u8))(i)?;
        match kind {
            GhwHieKind::Design => {
                error!("unsupported hierarchy kind: {:?}", kind);
                context("GhwHieKind:: unsupported hierarchy kind", fail)(next)
            }
            GhwHieKind::GenerateFor => {
                trace!("generate for");
                trace!("input is: {:?}", next);
                let (n, (name, iter_type)) = context(
                    "GhwHie Block name",
                    tuple((
                        map(
                            verify(uleb128_to_u::<u32, E>, |s_id| {
                                *s_id > 0 && *s_id as usize <= strs.len()
                            }),
                            |s_id| strs[s_id as usize - 1].clone(),
                        ),
                        map(
                            verify(uleb128_to_u::<u32, E>, |t_id| {
                                *t_id > 0 && *t_id as usize <= types.len()
                            }),
                            |t_id| types[t_id as usize - 1].clone(),
                        ),
                    )),
                )(next)?;
                next = n;
                let (n, iter_value) = GhwVal::parse(next, iter_type.clone(), endian)?;
                next = n;
                debug!("found generate for {}: {:?}", name, iter_value);
                IResult::Ok((
                    next,
                    Self {
                        kind,
                        parent: Some(parent),
                        name,
                        brother: RefCell::new(None),
                        body: GhwHieBody::Block(GhwHieBlock {
                            child: RefCell::new(None),
                            iter_type: Some(iter_type),
                            iter_value: Some(iter_value),
                        }),
                    },
                ))
            }
            GhwHieKind::Block
            | GhwHieKind::GenerateIf
            | GhwHieKind::Instance
            | GhwHieKind::Package => {
                let (next, name) = context(
                    "GhwHie Block name",
                    map(
                        verify(uleb128_to_u::<u32, E>, |s_id| {
                            *s_id > 0 && *s_id as usize <= strs.len()
                        }),
                        |s_id| strs[s_id as usize - 1].clone(),
                    ),
                )(next)?;
                trace!("found {} ({:?})", name, kind);
                IResult::Ok((
                    next,
                    Self {
                        kind,
                        parent: Some(parent),
                        name,
                        brother: RefCell::new(None),
                        body: GhwHieBody::Block(GhwHieBlock {
                            child: RefCell::new(None),
                            iter_type: None,
                            iter_value: None,
                        }),
                    },
                ))
            }
            GhwHieKind::Process => {
                let (next, name) = map(
                    verify(uleb128_to_u::<u32, E>, |s_id| {
                        *s_id > 0 && *s_id as usize <= strs.len()
                    }),
                    |s_id| strs[s_id as usize - 1].clone(),
                )(next)?;
                trace!("found {} ({:?})", name, kind);
                IResult::Ok((
                    next,
                    Self {
                        kind,
                        parent: Some(parent),
                        name,
                        brother: RefCell::new(None),
                        body: GhwHieBody::Block(GhwHieBlock {
                            child: RefCell::new(None),
                            iter_type: None,
                            iter_value: None,
                        }),
                    },
                ))
            }
            GhwHieKind::Generic => context("GhwHieKind::Generic not implemented yet", fail)(next),
            GhwHieKind::Eos | GhwHieKind::Eoh => IResult::Ok((
                next,
                Self {
                    kind,
                    parent: Some(parent),
                    name: Rc::new(String::from("")),
                    brother: RefCell::new(None),
                    body: GhwHieBody::Empty,
                },
            )),
            GhwHieKind::Signal
            | GhwHieKind::PortIn
            | GhwHieKind::PortOut
            | GhwHieKind::PortInout
            | GhwHieKind::PortBuffer
            | GhwHieKind::PortLinkage => {
                let (next, (name, ghw_type)) = context(
                    "GhwHie signal like block",
                    pair(
                        map(
                            verify(uleb128_to_u::<u32, E>, |s_id| {
                                *s_id > 0 && *s_id as usize <= strs.len()
                            }),
                            |s_id| strs[s_id as usize - 1].clone(),
                        ),
                        map(
                            verify(uleb128_to_u::<u32, E>, |t_id| {
                                *t_id > 0 && *t_id as usize <= types.len()
                            }),
                            |t_id| types[t_id as usize - 1].clone(),
                        ),
                    ),
                )(next)?;
                let num_el = match ghw_type.get_number_elements() {
                    Ok(n) => {
                        if n < 0 {
                            error!("{:?} has negative number of signal elements", ghw_type);
                            return context("hie sig: negative number of elements", fail)(next);
                        }
                        n
                    }
                    Err(_) => {
                        return context("hie sig: invalid number of elements", fail)(next);
                    }
                };
                trace!("found {} ({:?}) with {} elements", name, kind, num_el);
                let (next, sigs) = Self::read_signal(next, ghw_type.clone())?;
                IResult::Ok((
                    next,
                    Self {
                        kind,
                        parent: Some(parent),
                        name,
                        brother: RefCell::new(None),
                        body: GhwHieBody::Signal(GhwHieSignal {
                            ghwtype: ghw_type,
                            sigs,
                        }),
                    },
                ))
            }
        }
    }

    fn read_signal<'a, E>(
        i: &'a [u8],
        sig_type: Rc<GhwType>,
        //num_scalars: u32,
    ) -> IResult<&'a [u8], Vec<(u32, Rc<GhwType>)>, E>
    where
        E: ParseError<&'a [u8]>
            + ContextError<&'a [u8]>
            + std::fmt::Debug
            + FromExternalError<&'a [u8], GhwNumError>,
    {
        let mut sigs = Vec::new();
        match sig_type.deref() {
            GhwType::Enum(_) | GhwType::SubScalar(_) => {
                let (next, sig_el) = uleb128_to_u::<u32, E>(i)?;
                trace!("found scalar signal number {}", sig_el);
                sigs.push((sig_el, sig_type));
                IResult::Ok((next, sigs))
            }
            GhwType::Record(r) => {
                let nbr_fields = r.els.len();
                let mut next = i;
                for idx in 0..nbr_fields {
                    let (n, mut new_sigs) = Self::read_signal(next, r.els[idx].ghw_type.clone())?;
                    sigs.append(&mut new_sigs);
                    next = n;
                }
                Ok((next, sigs))
            }
            GhwType::SubArray(sa) => {
                if let Ok(n_el) = sig_type.get_number_elements() {
                    if n_el >= 0 {
                        let n_el = n_el as u32;
                        let mut next = i;
                        for _ in 0..n_el {
                            let (next_inner, mut nbrs) = Self::read_signal(next, sa.el.clone())?;
                            sigs.append(&mut nbrs);
                            next = next_inner;
                        }
                        IResult::Ok((next, sigs))
                    } else {
                        todo!()
                    }
                } else {
                    todo!();
                }
            }
            GhwType::SubRecord(sr) => {
                let mut next = i;
                let mut sigs = Vec::new();
                for field in &sr.els {

                    let (n, mut s_v) = Self::read_signal(next, field.ghw_type.clone())?;
                    next = n;
                    sigs.append(&mut s_v);
                }
                IResult::Ok((next, sigs))
            },
            _ => {
                error!("reading signal for hierarchy item {:?} not supported", sig_type);
                context("unexpected hierarchy item to read signals", fail)(i)
            },
        }
    }

    /// outputs a simple tree view on stdout
    pub fn print_tree(item: Rc<Self>, recurse_idx: usize, recurse_limit: usize) {
        let mut indent = String::from("");
        if recurse_idx > 0 {
            for _ in 0..recurse_idx - 1 {
                indent.push_str("| ");
            }
        }
        let initial_indent = match recurse_idx {
            0 => "",
            _ => "|-",
        };
        match &item.body {
            GhwHieBody::Block(b) => {
                println!(
                    "{}{}{} ({:?})",
                    indent, initial_indent, item.name, item.kind
                );
                if recurse_idx < recurse_limit {
                    match b.child.borrow().deref() {
                        Some(child) => {
                            Self::print_tree(child.clone(), recurse_idx + 1, recurse_limit)
                        }
                        None => {}
                    }
                }
            }
            GhwHieBody::Signal(s) => {
                let sig_str = if s.sigs.is_empty() {
                    String::from("<empty>")
                } else if s.sigs.len() == 1 {
                    format!("{}", s.sigs[0].0)
                } else if s.sigs.last().unwrap().0 as i64 - s.sigs.first().unwrap().0 as i64
                    == s.sigs.len() as i64 - 1
                {
                    format!(
                        "{} to {}",
                        s.sigs.first().unwrap().0,
                        s.sigs.last().unwrap().0
                    )
                } else if s.sigs.first().unwrap().0 as i64 - s.sigs.last().unwrap().0 as i64
                    == s.sigs.len() as i64 - 1
                {
                    format!(
                        "{} down to {}",
                        s.sigs.first().unwrap().0,
                        s.sigs.last().unwrap().0
                    )
                } else {
                    String::from("todo")
                };
                println!(
                    "{}{}{} ({:?}), sigs: {}",
                    indent, initial_indent, item.name, item.kind, sig_str
                );
            }
            _ => {
                println!(
                    "{}{}{} ({:?})",
                    indent, initial_indent, item.name, item.kind
                );
            }
        }
        match item.brother.borrow().deref() {
            Some(b) => Self::print_tree(b.clone(), recurse_idx, recurse_limit),
            None => {}
        }
    }

    pub fn child(&self) -> Option<Rc<GhwHie>> {
        match &self.body {
            GhwHieBody::Block(b) => b.child.clone().into_inner(),
            GhwHieBody::Signal(_) => None,
            GhwHieBody::Empty => None,
        }
    }

    pub fn brother(&self) -> Option<Rc<GhwHie>> {
        self.brother.clone().into_inner()
    }

    pub fn parent(&self) -> Option<Rc<GhwHie>> {
        self.parent.clone()
    }
}

/// implemented to prevent infinite recursion at parent
impl Debug for GhwHie {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let parent_name = match &self.parent {
            Some(p) => p.name.deref().clone(),
            None => String::from("Empty"),
        };
        f.debug_struct("GhwHie")
            .field("kind", &self.kind)
            .field("parent", &parent_name)
            .field("name", &self.name)
            .field("brother", &self.brother)
            .field("body", &self.body)
            .finish()
    }
}

#[doc(hidden)]
#[cfg(test)]
mod tests {
    use nom::{number::Endianness, IResult};

    use crate::sections::{ghwhierarchy::GhwHierarchy, ghwstrings::Strings, ghwtypes::GhwTypes};

    const TYPE_STRINGS_RAW: &[u8] = &[
        0x53, 0x54, 0x52, 0x00, 0x00, 0x00, 0x00, 0x00, 0x2f, 0x00, 0x00, 0x00, 0x34, 0x01, 0x00,
        0x00, 0x27, 0x2d, 0x27, 0x01, 0x30, 0x27, 0x01, 0x31, 0x27, 0x01, 0x48, 0x27, 0x01, 0x4c,
        0x27, 0x01, 0x55, 0x27, 0x01, 0x57, 0x27, 0x01, 0x58, 0x27, 0x01, 0x5a, 0x27, 0x00, 0x50,
        0x30, 0x00, 0x61, 0x00, 0x62, 0x01, 0x73, 0x01, 0x75, 0x73, 0x5f, 0x73, 0x74, 0x61, 0x74,
        0x65, 0x01, 0x79, 0x74, 0x65, 0x00, 0x63, 0x6c, 0x6b, 0x00, 0x64, 0x61, 0x74, 0x61, 0x00,
        0x65, 0x72, 0x72, 0x6f, 0x72, 0x5f, 0x70, 0x72, 0x6f, 0x62, 0x00, 0x69, 0x6e, 0x74, 0x65,
        0x67, 0x65, 0x72, 0x00, 0x6d, 0x5f, 0x62, 0x75, 0x73, 0x01, 0x61, 0x69, 0x6e, 0x5f, 0x62,
        0x75, 0x73, 0x01, 0x65, 0x6d, 0x03, 0x6f, 0x72, 0x79, 0x00, 0x6e, 0x61, 0x74, 0x75, 0x72,
        0x61, 0x6c, 0x01, 0x75, 0x6d, 0x5f, 0x69, 0x6e, 0x74, 0x04, 0x70, 0x6f, 0x73, 0x04, 0x73,
        0x6d, 0x61, 0x6c, 0x6c, 0x5f, 0x69, 0x6e, 0x74, 0x03, 0x65, 0x72, 0x69, 0x63, 0x5f, 0x73,
        0x74, 0x64, 0x00, 0x6f, 0x68, 0x6d, 0x00, 0x70, 0x6f, 0x73, 0x69, 0x74, 0x69, 0x76, 0x65,
        0x01, 0x72, 0x6f, 0x62, 0x61, 0x62, 0x69, 0x6c, 0x69, 0x74, 0x79, 0x03, 0x67, 0x5f, 0x73,
        0x74, 0x61, 0x74, 0x65, 0x04, 0x72, 0x65, 0x73, 0x73, 0x5f, 0x73, 0x74, 0x61, 0x74, 0x65,
        0x00, 0x72, 0x65, 0x73, 0x69, 0x73, 0x74, 0x61, 0x6e, 0x63, 0x65, 0x00, 0x73, 0x5f, 0x61,
        0x63, 0x74, 0x69, 0x6f, 0x6e, 0x02, 0x69, 0x64, 0x6c, 0x65, 0x03, 0x6e, 0x69, 0x74, 0x01,
        0x68, 0x75, 0x6e, 0x74, 0x01, 0x6d, 0x61, 0x6c, 0x6c, 0x5f, 0x69, 0x6e, 0x74, 0x01, 0x74,
        0x61, 0x6e, 0x64, 0x61, 0x72, 0x64, 0x03, 0x74, 0x65, 0x5f, 0x63, 0x6f, 0x75, 0x6e, 0x74,
        0x73, 0x02, 0x64, 0x5f, 0x6c, 0x6f, 0x67, 0x69, 0x63, 0x09, 0x5f, 0x31, 0x31, 0x36, 0x34,
        0x0a, 0x76, 0x65, 0x63, 0x74, 0x6f, 0x72, 0x04, 0x75, 0x6c, 0x6f, 0x67, 0x69, 0x63, 0x00,
        0x74, 0x62, 0x5f, 0x73, 0x69, 0x67, 0x6e, 0x61, 0x6c, 0x73, 0x01, 0x63, 0x00, 0x45, 0x4f,
        0x53, 0x00,
    ];
    const TYPE_DATA: &[u8] = &[
        0x54, 0x59, 0x50, 0x00, 0x00, 0x00, 0x00, 0x00, 0x16, 0x00, 0x00, 0x00, 0x19, 0x13, 0x22,
        0x13, 0x01, 0x19, 0x80, 0x80, 0x80, 0x80, 0x78, 0xff, 0xff, 0xff, 0xff, 0x07, 0x22, 0x1e,
        0x01, 0x19, 0x01, 0xff, 0xff, 0xff, 0xff, 0x07, 0x22, 0x27, 0x01, 0x19, 0x80, 0x7f, 0xff,
        0x00, 0x1b, 0x1f, 0x22, 0x1f, 0x05, 0x1b, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xf0, 0x3f, 0x1c, 0x22, 0x01, 0x1d, 0x01, 0x22, 0x22,
        0x07, 0x1c, 0x00, 0x80, 0x94, 0xeb, 0xdc, 0x03, 0x17, 0x2d, 0x09, 0x06, 0x08, 0x02, 0x03,
        0x09, 0x07, 0x05, 0x04, 0x01, 0x22, 0x2a, 0x09, 0x17, 0x00, 0x08, 0x17, 0x20, 0x03, 0x25,
        0x24, 0x23, 0x22, 0x18, 0x01, 0x19, 0x00, 0xff, 0xff, 0xff, 0xff, 0x07, 0x1f, 0x2c, 0x0a,
        0x01, 0x0c, 0x23, 0x00, 0x0d, 0x99, 0x01, 0x00, 0x1f, 0x0f, 0x0a, 0x01, 0x02, 0x23, 0x0f,
        0x0f, 0x99, 0x07, 0x00, 0x1f, 0x29, 0x0c, 0x01, 0x0b, 0x23, 0x29, 0x11, 0x17, 0x00, 0x02,
        0x1f, 0x17, 0x10, 0x01, 0x02, 0x23, 0x17, 0x13, 0x99, 0x03, 0x00, 0x23, 0x00, 0x0d, 0x99,
        0x07, 0x00, 0x20, 0x15, 0x03, 0x10, 0x0a, 0x0e, 0x0b, 0x11, 0x15, 0x00, 0x57, 0x4b, 0x54,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x09, 0x00,
    ];
    const HIE_DATA: &[u8] = &[
        0x48, 0x49, 0x45, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x0c, 0x00, 0x00,
        0x00, 0x3e, 0x00, 0x00, 0x00, 0x07, 0x28, 0x0f, 0x07, 0x2b, 0x0f, 0x07, 0x1c, 0x0f, 0x06,
        0x2e, 0x10, 0x19, 0x02, 0x01, 0x10, 0x1a, 0x03, 0x02, 0x10, 0x1b, 0x04, 0x03, 0x10, 0x12,
        0x06, 0x04, 0x10, 0x26, 0x08, 0x05, 0x10, 0x0b, 0x0a, 0x06, 0x10, 0x21, 0x0b, 0x07, 0x10,
        0x0c, 0x0e, 0x08, 0x09, 0x10, 0x0d, 0x10, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11,
        0x10, 0x2f, 0x12, 0x12, 0x13, 0x14, 0x10, 0x16, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a,
        0x1b, 0x1c, 0x1d, 0x1e, 0x1f, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29,
        0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f, 0x30, 0x31, 0x32, 0x33, 0x34, 0x10, 0x14, 0x16, 0x35,
        0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d, 0x3e, 0x0d, 0x0a, 0x0f, 0x00,
    ];
    #[test]
    fn verify_1() {
        let strs: IResult<_, _, nom::error::VerboseError<_>> = Strings::parse(TYPE_STRINGS_RAW);
        let strs = strs.unwrap().1;
        println!("strings: {:#?}", strs);
        let types: IResult<_, _, nom::error::VerboseError<_>> =
            GhwTypes::parse(TYPE_DATA, strs.strs.clone(), Endianness::Little);
        let types = types.unwrap().1;
        println!("============================");
        let hie: IResult<_, _, nom::error::VerboseError<_>> =
            GhwHierarchy::parse(HIE_DATA, strs.strs, types.type_table, Endianness::Little);
        println!("============================");
        let hie = hie.unwrap().1;
        println!("hierarchy: {:?}", hie);
        //assert!(false, "not implemented yet");
    }
}
