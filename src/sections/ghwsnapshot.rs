use nom::{
    bytes::complete::tag,
    combinator::fail,
    error::{context, ContextError, FromExternalError, ParseError},
    number::complete::i64,
    number::Endianness,
    sequence::preceded,
    IResult,
};

use crate::{common::GhwVal, error::GhwNumError};

use super::ghwhierarchy::GhwHierarchy;

/// initial values of all signals. Will be read at object construction
#[derive(Debug)]
pub struct Snapshot {
    pub timestamp: i64,
    pub values: Vec<GhwVal>,
}

impl Snapshot {
    pub fn parse<'a, E>(
        i: &'a [u8],
        hie: &mut GhwHierarchy,
        endian: Endianness,
    ) -> IResult<&'a [u8], Self, E>
    where
        E: ParseError<&'a [u8]>
            + ContextError<&'a [u8]>
            + std::fmt::Debug
            + FromExternalError<&'a [u8], GhwNumError>,
    {
        let mut next = i;
        let mut values = Vec::new();
        let (n, timestamp) = context(
            "Snapshot",
            preceded(context("magic_header", tag(b"SNP\0")), i64(endian)),
        )(next)?;
        next = n;
        let sig_types = hie.get_signal_types();
        let sig_types = match sig_types {
            Some(st) => st,
            None => {
                return context("could not get signal types", fail)(next);
            }
        };
        for sig_type in sig_types {
            let (n, v) = GhwVal::parse(next, sig_type, endian)?;
            values.push(v);
            next = n;
        }
        IResult::Ok((next, Self { timestamp, values }))
    }
}
