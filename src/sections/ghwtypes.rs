use std::{cell::RefCell, fmt::Debug, ops::Deref, rc::Rc};

use nom::{
    bytes::complete::tag,
    combinator::{fail, map, map_opt, verify},
    error::{context, ContextError, FromExternalError, ParseError},
    multi::{length_count, many_till},
    number::{
        complete::{f64, i32, le_u8},
        Endianness,
    },
    sequence::{pair, preceded, tuple},
    IResult,
};
use num::FromPrimitive;
use svg::node::element;
use tracing::{debug, error, info, span, trace, warn, Level};

use crate::{
    common::{lsleb128_to_i64, sleb128_to_i32, uleb128_to_u, GhdlRtik, RtiKind},
    error::GhwNumError,
};

//use num_traits::{FromPrimitive, ToPrimitive};

/// container for all types
#[derive(Debug, Clone)]
pub struct GhwTypes {
    pub type_table: Rc<Vec<Rc<GhwType>>>,
}

#[derive(Debug, Clone)]
/// equivalent of ghw_type C union
pub enum GhwType {
    Common(GhwTypeCommon),
    Enum(GhwTypeEnum),
    Scalar(GhwTypeScalar),
    Physical(GhwTypePhysical),
    SubScalar(GhwSubtypeScalar),
    Array(GhwTypeArray),
    Record(GhwTypeRecord),
    SubArray(GhwSubtypeArray),
    SubUnboundedArray(GhwSubtypeUnboundedArray),
    SubRecord(GhwSubtypeRecord),
    SubUnboundedRecord(GhwSubtypeUnboundedRecord),
}

// bodies of ghw_type enum/union

#[derive(Debug, Clone)]
pub struct GhwTypeCommon {
    kind: GhdlRtik,
    pub name: Rc<String>,
}

#[derive(Debug, Clone)]
pub struct GhwTypeEnum {
    pub kind: GhdlRtik,
    pub name: Rc<String>,
    /// is this type a well known type?
    pub wkt: RefCell<GhwWktType>,
    /// literals of the values, which this enum can take on
    pub lits: Vec<Rc<String>>,
}

#[derive(Debug, Clone)]
pub struct GhwTypeScalar {
    pub kind: GhdlRtik,
    pub name: Rc<String>,
}

#[derive(Debug, Clone)]
pub struct GhwTypePhysical {
    pub kind: GhdlRtik,
    pub name: Rc<String>,
    /// the first unit is the primary unit. All other units are secondary and must be
    /// an integral multiple of the primary unit (or indirectly via an other secondary
    /// unit)
    pub units: Rc<Vec<Rc<GhwUnit>>>,
}

#[derive(Debug, Clone)]
pub struct GhwSubtypeScalar {
    pub kind: GhdlRtik,
    pub name: Rc<String>,
    /// base type
    pub base: Rc<GhwType>,
    /// range of its base type
    pub rng: GhwRange,
}

#[derive(Debug, Clone)]
pub struct GhwTypeArray {
    kind: GhdlRtik,
    pub name: Rc<String>,
    /// type of elements
    pub el: Rc<GhwType>,
    /// dimensions
    pub dims: Vec<Rc<GhwType>>,
}

#[derive(Debug, Clone)]
pub struct GhwSubtypeUnboundedArray {
    kind: GhdlRtik,
    pub name: Rc<String>,
    /// base type
    pub base: Rc<GhwType>,
}

#[derive(Debug, Clone)]
pub struct GhwSubtypeArray {
    kind: GhdlRtik,
    pub name: Rc<String>,
    pub base: Rc<GhwType>,
    /// ?
    pub nbr_scalars: i32,
    /// ranges of elements, ?only scalars? TODO
    pub ranges: Vec<Rc<GhwRange>>,
    /// type of elements
    pub el: Rc<GhwType>,
}

/// TODO clarify field purposes in doc
#[derive(Debug, Clone)]
pub struct GhwTypeRecord {
    kind: GhdlRtik,
    pub name: Rc<String>,
    /// number of scalar elements
    pub nbr_scalars: i32,
    pub els: Vec<Rc<GhwRecordElement>>,
}

/// TODO clarify field purposes in doc
#[derive(Debug, Clone)]
pub struct GhwSubtypeRecord {
    kind: GhdlRtik,
    pub name: Rc<String>,
    pub base: Rc<GhwType>,
    pub nbr_scalars: i32,
    pub els: Vec<Rc<GhwRecordElement>>,
}

/// TODO clarify field purposes in doc
#[derive(Debug, Clone)]
pub struct GhwSubtypeUnboundedRecord {
    kind: GhdlRtik,
    pub name: Rc<String>,
    pub base: Rc<GhwType>,
}

// helper struct for bodies
#[derive(Copy, Clone, Debug, Primitive)]
#[repr(u8)]
/// equivalent to C enum ghw_wkt_type
/// wkt: well known types
pub enum GhwWktType {
    Unknown = 0,
    Boolean = 1,
    Bit = 2,
    StdUlogic = 3,
}

/// The range of different elements in a GhwType
#[derive(Debug, Clone)]
pub enum GhwRange {
    B2(GhwRangeType<u8>),
    E8(GhwRangeType<u8>),
    I32(GhwRangeType<i32>),
    I64(GhwRangeType<i64>),
    F64(GhwRangeType<f64>),
}

/// Template for Enum variants of GhwRange
#[derive(Debug, Clone)]
pub struct GhwRangeType<T: Copy + Clone + Debug> {
    pub kind: GhdlRtik,
    /// direction of the range boundary
    pub dir: GhwDir,
    /// left range boundary
    pub left: T,
    /// right range boundary
    pub right: T,
}

/// direction (of a range): int dir:8 in C
#[derive(Clone, Copy, Debug)]
#[repr(u8)]
pub enum GhwDir {
    /// incrementing
    To = 0,
    /// decrementing
    Downto = 0x80,
}

/// Value of a GhwUnit including unit name
#[derive(Debug, Clone)]
pub struct GhwUnit {
    pub name: Rc<String>,
    pub val: i64,
}

/// an element of an GhwRecord
#[derive(Debug, Clone)]
pub struct GhwRecordElement {
    pub name: Rc<String>,
    pub ghw_type: Rc<GhwType>,
}

// implementations
impl GhwTypes {
    pub fn parse<'a, E>(
        i: &'a [u8],
        strs: Rc<Vec<Rc<String>>>,
        endian: Endianness,
    ) -> IResult<&'a [u8], Self, E>
    where
        E: ParseError<&'a [u8]>
            + ContextError<&'a [u8]>
            + std::fmt::Debug
            + FromExternalError<&'a [u8], GhwNumError>,
    {
        let fn_span = span!(Level::INFO, "types");
        let _enter = fn_span.enter();
        let mut next = i;
        let (n, num_types) = context(
            "ghw types section header",
            preceded(
                context("magic header", pair(tag(b"TYP\0"), tag([0, 0, 0, 0]))),
                context("number of GhwTypes", i32(endian)),
            ),
        )(next)?;
        info!("expecting {} types to parse", num_types);
        next = n;
        let mut types = Vec::new();
        for idx in 0..num_types {
            trace!("parsing type number {}", idx + 1);
            let (n, new_type) = GhwType::parse(next, strs.clone(), &types, endian)?;
            types.push(Rc::new(new_type));
            next = n;
        }
        let (n, _) = context("Types closing zero not found", tag(&[0]))(next)?;
        next = n;
        let (n, _) = context(
            "well known types",
            pair(
                pair(tag(b"WKT\0"), tag(&[0, 0, 0, 0])),
                many_till(
                    map(
                        pair(
                            map(le_u8, |wkt_id| match GhwWktType::from_u8(wkt_id) {
                                Some(wkt) => wkt,
                                None => GhwWktType::Unknown,
                            }),
                            verify(uleb128_to_u::<u32, E>, |t_id| {
                                *t_id > 0 && *t_id as usize <= types.len()
                            }),
                        ),
                        |(wkt, t_id)| match types[t_id as usize - 1].deref() {
                            GhwType::Enum(e) => {
                                e.wkt.replace(wkt);
                            }
                            _ => {
                                println!("Warning: incompatible for wkt type");
                            }
                        },
                    ),
                    tag(&[0]),
                ),
            ),
        )(next)?;
        next = n;
        if next.len() > 0 {
            warn!("remaining input: {:?}", next);
        }
        IResult::Ok((
            next,
            GhwTypes {
                type_table: Rc::new(types),
            },
        ))
    }
}

impl GhwType {
    pub fn parse<'a, E>(
        i: &'a [u8],
        strs: Rc<Vec<Rc<String>>>,
        parsed_types: &Vec<Rc<GhwType>>,
        endian: Endianness,
    ) -> IResult<&'a [u8], Self, E>
    where
        E: ParseError<&'a [u8]>
            + ContextError<&'a [u8]>
            + std::fmt::Debug
            + FromExternalError<&'a [u8], GhwNumError>,
    {
        /*println!(
            "====== GhwType::parse(\n\t{:?},\n\t{:?},\n\t{:?}\n) ======",
            strs, parsed_types, endian
        );*/
        let (next_type_data, kind) = context(
            "identify kind of GhwType item",
            map_opt(le_u8, GhdlRtik::from_u8),
        )(i)?;
        trace!("parsing type of kind {:?}", kind);
        match kind {
            GhdlRtik::Top => context("GhdlRtik::Top not implemented", fail)(next_type_data),
            GhdlRtik::Library => context("GhdlRtik::Library not implemented", fail)(next_type_data),
            GhdlRtik::Package => context("GhdlRtik::Package not implemented", fail)(next_type_data),
            GhdlRtik::PackageBody => {
                context("GhdlRtik::PackageBody not implemented", fail)(next_type_data)
            }
            GhdlRtik::Entity => context("GhdlRtik::Entity not implemented", fail)(next_type_data),
            GhdlRtik::Architecture => {
                context("GhdlRtik::Architecture not implemented", fail)(next_type_data)
            }
            GhdlRtik::Process => context("GhdlRtik::Process not implemented", fail)(next_type_data),
            GhdlRtik::Block => context("GhdlRtik::Block not implemented", fail)(next_type_data),
            GhdlRtik::IfGenerate => {
                context("GhdlRtik::IfGenerate not implemented", fail)(next_type_data)
            }
            GhdlRtik::ForGenerate => {
                context("GhdlRtik::ForGenerate not implemented", fail)(next_type_data)
            }
            GhdlRtik::Instance => {
                context("GhdlRtik::Instance not implemented", fail)(next_type_data)
            }
            GhdlRtik::Constant => {
                context("GhdlRtik::Constant not implemented", fail)(next_type_data)
            }
            GhdlRtik::Iterator => {
                context("GhdlRtik::Iterator not implemented", fail)(next_type_data)
            }
            GhdlRtik::Variable => {
                context("GhdlRtik::Variable not implemented", fail)(next_type_data)
            }
            GhdlRtik::Signal => context("GhdlRtik::Signal not implemented", fail)(next_type_data),
            GhdlRtik::File => context("GhdlRtik::File not implemented", fail)(next_type_data),
            GhdlRtik::Port => context("GhdlRtik::Port not implemented", fail)(next_type_data),
            GhdlRtik::Generic => context("GhdlRtik::Generic not implemented", fail)(next_type_data),
            GhdlRtik::Alias => context("GhdlRtik::Alias not implemented", fail)(next_type_data),
            GhdlRtik::Guard => context("GhdlRtik::Guard not implemented", fail)(next_type_data),
            GhdlRtik::Component => {
                context("GhdlRtik::Component not implemented", fail)(next_type_data)
            }
            GhdlRtik::Attribute => {
                context("GhdlRtik::Attribute not implemented", fail)(next_type_data)
            }
            GhdlRtik::TypeB2 => context("GhdlRtik::TypeB2 not implemented", fail)(next_type_data),
            GhdlRtik::TypeE8 => {
                let (next, (name_str_id, lits)) = context(
                    "GhdlRtik::TypeE8",
                    pair(
                        context(
                            "string id",
                            verify(uleb128_to_u::<u32, E>, |id| {
                                *id > 0 || *id as usize <= strs.len()
                            }),
                        ),
                        length_count(
                            context("number of elements", uleb128_to_u::<u32, E>),
                            context(
                                "literal string id",
                                map(
                                    verify(uleb128_to_u::<u32, E>, |id| {
                                        *id > 0 && *id as usize <= strs.len()
                                    }),
                                    |lit_id| strs[lit_id as usize - 1].clone(),
                                ),
                            ),
                        ),
                    ),
                )(next_type_data)?;
                IResult::Ok((
                    next,
                    Self::Enum(GhwTypeEnum {
                        kind,
                        name: strs[name_str_id as usize - 1].clone(),
                        wkt: RefCell::new(GhwWktType::Unknown),
                        lits,
                    }),
                ))
            }
            GhdlRtik::TypeE32 => context("GhdlRtik::TypeE32 not implemented", fail)(next_type_data),
            GhdlRtik::TypeI32 | GhdlRtik::TypeI64 | GhdlRtik::TypeF64 => {
                let str_id_raw = context("GhwType F64/I32/I64: string id", uleb128_to_u::<u32, E>)(
                    next_type_data,
                )?;
                if str_id_raw.1 <= 1 {
                    return context("unexpected zero string id", fail)(str_id_raw.0);
                }
                match strs.get(str_id_raw.1 as usize - 1) {
                    Some(str_id) => IResult::Ok((
                        str_id_raw.0,
                        Self::Scalar(GhwTypeScalar {
                            kind,
                            name: str_id.clone(),
                        }),
                    )),
                    None => fail(str_id_raw.0),
                }
            }
            GhdlRtik::TypeP32 | GhdlRtik::TypeP64 => {
                let (next, (name, units)) = context(
                    "GhdlRtik::TypeP32/64",
                    pair(
                        context(
                            "name",
                            map(
                                verify(uleb128_to_u::<u32, E>, |id| {
                                    *id > 0 && *id as usize <= strs.len()
                                }),
                                |id| strs[id as usize - 1].clone(),
                            ),
                        ),
                        context(
                            "P32: unit array",
                            length_count(
                                context("units count", uleb128_to_u::<u32, E>),
                                context(
                                    "unit content",
                                    map_opt(
                                        pair(
                                            context("unit string id", uleb128_to_u::<u32, E>),
                                            context("unit pointer ?!?", lsleb128_to_i64::<E>),
                                        ),
                                        |(str_id, val)| {
                                            if str_id >= 1 && str_id as usize <= strs.len() {
                                                Some(Rc::new(GhwUnit {
                                                    name: strs[str_id as usize - 1].clone(),
                                                    val,
                                                }))
                                            } else {
                                                None
                                            }
                                        },
                                    ),
                                ),
                            ),
                        ),
                    ),
                )(next_type_data)?;
                IResult::Ok((
                    next,
                    Self::Physical(GhwTypePhysical {
                        kind,
                        name,
                        units: Rc::new(units),
                    }),
                ))
            }
            GhdlRtik::TypeAccess => {
                context("GhdlRtik::TypeAccess not implemented", fail)(next_type_data)
            }
            GhdlRtik::TypeArray => {
                let (next, (name, el, dims)) = tuple((
                    context(
                        "GhwType::Array name",
                        map(
                            verify(uleb128_to_u::<u32, E>, |str_id| {
                                *str_id > 0 && *str_id as usize <= strs.len()
                            }),
                            |str_id| strs[str_id as usize - 1].clone(),
                        ),
                    ),
                    context(
                        "GhwType::Array element type",
                        map(
                            verify(uleb128_to_u::<u32, E>, |type_id| {
                                *type_id > 0 && *type_id as usize <= parsed_types.len()
                            }),
                            |type_id| parsed_types[type_id as usize - 1].clone(),
                        ),
                    ),
                    context(
                        "GhwType::Array dims",
                        length_count(
                            context("dims length", uleb128_to_u::<u32, E>),
                            context(
                                "GhwType::Array dim type",
                                map(
                                    verify(uleb128_to_u::<u32, E>, |t_id| {
                                        *t_id > 0 && *t_id as usize <= parsed_types.len()
                                    }),
                                    |t_id| parsed_types[t_id as usize - 1].clone(),
                                ),
                            ),
                        ),
                    ),
                ))(next_type_data)?;
                IResult::Ok((
                    next,
                    Self::Array(GhwTypeArray {
                        kind,
                        name,
                        el,
                        dims,
                    }),
                ))
            }
            GhdlRtik::TypeRecord => {
                let mut nbr_scalars = 0;
                let (next, (name, fields)) = context(
                    "TypeRecord",
                    pair(
                        context(
                            "name",
                            map(
                                verify(uleb128_to_u::<u32, E>, |id| {
                                    *id > 0 && *id as usize <= strs.len()
                                }),
                                |id| strs[id as usize - 1].clone(),
                            ),
                        ),
                        context(
                            "fields",
                            length_count(
                                context("nbr of fields", uleb128_to_u::<u32, E>),
                                context(
                                    "field item",
                                    map(
                                        pair(
                                            context(
                                                "name",
                                                map(
                                                    verify(uleb128_to_u::<u32, E>, |id| {
                                                        *id > 0 && *id as usize <= strs.len()
                                                    }),
                                                    |id| strs[id as usize - 1].clone(),
                                                ),
                                            ),
                                            context(
                                                "type",
                                                map(
                                                    verify(uleb128_to_u::<u32, E>, |id| {
                                                        *id > 0
                                                            && *id as usize <= parsed_types.len()
                                                    }),
                                                    |id| parsed_types[id as usize - 1].clone(),
                                                ),
                                            ),
                                        ),
                                        |(name, element)| {
                                            Rc::new(GhwRecordElement {
                                                name,
                                                ghw_type: element,
                                            })
                                        },
                                    ),
                                ),
                            ),
                        ),
                    ),
                )(next_type_data)?;
                for field in &fields {
                    if nbr_scalars != -1 {
                        if let Ok(field_nbr_scalars) = field.ghw_type.get_number_elements() {
                            if field_nbr_scalars == -1 {
                                nbr_scalars = -1;
                            } else {
                                nbr_scalars += field_nbr_scalars;
                            }
                        } else {
                            return context(
                                "TypeRecord: could not get number of elements of a field",
                                fail,
                            )(next);
                        }
                    } else {
                        break;
                    };
                }
                IResult::Ok((
                    next,
                    Self::Record(GhwTypeRecord {
                        kind,
                        name,
                        nbr_scalars,
                        els: fields,
                    }),
                ))
            }
            GhdlRtik::TypeFile => {
                context("GhdlRtik::TypeFile not implemented", fail)(next_type_data)
            }
            GhdlRtik::SubtypeScalar => {
                let (type_content_next, (name, base)) = context(
                    "GhwType",
                    tuple((
                        context(
                            "string",
                            map(
                                verify(uleb128_to_u::<u32, E>, |s_id| *s_id as usize <= strs.len()),
                                |s_id| {
                                    if s_id == 0 {
                                        warn!("empty name for a type of kind {:?}", kind);
                                        Rc::new(String::from(""))
                                    } else {
                                        strs[s_id as usize - 1].clone()
                                    }
                                },
                            ),
                        ),
                        context(
                            "base type",
                            map(
                                verify(uleb128_to_u::<u32, E>, |t_id| {
                                    *t_id > 0 && *t_id as usize <= parsed_types.len()
                                }),
                                |t_id| parsed_types[t_id as usize - 1].clone(),
                            ),
                        ),
                    )),
                )(next_type_data)?;
                match base.deref() {
                    GhwType::Scalar(_s) => {
                        let range = GhwRange::parse(type_content_next, endian)?;
                        IResult::Ok((
                            range.0,
                            Self::SubScalar(GhwSubtypeScalar {
                                kind,
                                name,
                                base,
                                rng: range.1,
                            }),
                        ))
                    }
                    GhwType::Physical(_p) => {
                        let range = GhwRange::parse(type_content_next, endian)?;
                        IResult::Ok((
                            range.0,
                            Self::SubScalar(GhwSubtypeScalar {
                                kind,
                                name,
                                base,
                                rng: range.1,
                            }),
                        ))
                    }
                    GhwType::Enum(_e) => {
                        let range = GhwRange::parse(type_content_next, endian)?;
                        IResult::Ok((
                            range.0,
                            Self::SubScalar(GhwSubtypeScalar {
                                kind,
                                name,
                                base,
                                rng: range.1,
                            }),
                        ))
                    }
                    _ => panic!("subtype scalar not implemented for {:?}", base),
                }
            }
            GhdlRtik::SubtypeArray => {
                let (next, (name, base)) = context(
                    "outer SubtypeArray",
                    tuple((
                        context(
                            "outer name",
                            map(
                                verify(uleb128_to_u::<u32, E>, |id| *id as usize <= strs.len()),
                                |id| {
                                    if id == 0 {
                                        Rc::new(String::from(""))
                                    } else {
                                        strs[id as usize - 1].clone()
                                    }
                                },
                            ),
                        ),
                        context(
                            "outer base type",
                            map(
                                verify(uleb128_to_u::<u32, E>, |id| {
                                    *id > 0 && *id as usize <= parsed_types.len()
                                }),
                                |id| parsed_types[id as usize - 1].clone(),
                            ),
                        ),
                    )),
                )(next_type_data)?;
                let (next, sa) =
                    Self::read_array_subtype(next, base.clone(), strs, parsed_types, endian)?;
                if let GhwType::SubArray(mut sa) = sa {
                    sa.name = name;
                    sa.base = base;
                    IResult::Ok((next, Self::SubArray(sa)))
                } else {
                    context("outer subtype array parsed to wrong header", fail)(next)
                }
            }
            GhdlRtik::SubtypeArrayPtr => {
                context("GhdlRtik::SubtypeArrayPtr not implemented", fail)(next_type_data)
            }
            GhdlRtik::SubtypeUnboundedArray => {
                let (next, (name, base)) = pair(
                    map(
                        verify(uleb128_to_u::<u32, E>, |s_id| *s_id as usize <= strs.len()),
                        |s_id| {
                            if s_id == 0 {
                                Rc::new(String::new())
                            } else {
                                strs[s_id as usize - 1].clone()
                            }
                        },
                    ),
                    map(
                        verify(uleb128_to_u::<u32, E>, |t_id| {
                            *t_id > 0 && *t_id as usize <= parsed_types.len()
                        }),
                        |t_id| parsed_types[t_id as usize - 1].clone(),
                    ),
                )(next_type_data)?;
                IResult::Ok((
                    next,
                    Self::SubUnboundedArray(GhwSubtypeUnboundedArray { kind, name, base }),
                ))
            }
            GhdlRtik::SubtypeRecord => {
                let (next, (name, base)) = pair(
                    map(
                        verify(uleb128_to_u::<u32, E>, |s_id| *s_id as usize <= strs.len()),
                        |s_id| {
                            if s_id == 0 {
                                Rc::new(String::new())
                            } else {
                                strs[s_id as usize - 1].clone()
                            }
                        },
                    ),
                    map(
                        verify(uleb128_to_u::<u32, E>, |t_id| {
                            *t_id > 0 && *t_id as usize <= parsed_types.len()
                        }),
                        |t_id| parsed_types[t_id as usize - 1].clone(),
                    ),
                )(next_type_data)?;
                let(next, mut sr) = Self::read_record_subtype(next, base, strs, parsed_types, endian)?;
                sr.name = name;
                trace!("record {}:", sr.name);
                IResult::Ok((next, Self::SubRecord(sr)))
            }
            GhdlRtik::SubtypeUnboundedRecord => {
                context("GhdlRtik::SubtypeUnboundedRecord not implemented", fail)(next_type_data)
            }
        }
    }

    pub fn get_number_elements(&self) -> Result<i32, GhwNumError> {
        match self {
            GhwType::Common(_) => todo!(),
            GhwType::Enum(_) => Ok(1),
            GhwType::Scalar(_) => Ok(1),
            GhwType::Physical(_) => Ok(1),
            GhwType::SubScalar(_) => Ok(1),
            GhwType::Array(_) => Ok(-1),
            GhwType::Record(r) => Ok(r.nbr_scalars),
            GhwType::SubArray(sa) => Ok(sa.nbr_scalars),
            GhwType::SubUnboundedArray(_) => Ok(-1),
            GhwType::SubRecord(sr) => Ok(sr.nbr_scalars),
            GhwType::SubUnboundedRecord(_) => Ok(-1),
        }
    }

    fn read_array_subtype<'a, E>(
        next: &'a [u8],
        base: Rc<Self>,
        strs: Rc<Vec<Rc<String>>>,
        parsed_types: &Vec<Rc<GhwType>>,
        endian: Endianness,
    ) -> IResult<&'a [u8], Self, E>
    where
        E: ParseError<&'a [u8]>
            + ContextError<&'a [u8]>
            + std::fmt::Debug
            + FromExternalError<&'a [u8], GhwNumError>,
    {
        let btype = match Self::base_type(&base) {
            Some(bt) => bt,
            None => {
                error!("expected GhwType::Array(.) as inner base, but found none");
                return context("inner base for read_array_subtype", fail)(next);
            }
        };
        let inner_base = match btype.deref() {
            GhwType::Array(ar) => Rc::new(ar.clone()),
            _ => {
                error!(
                    "expected GhwType::Array(.) as inner base, but found {:?}",
                    base
                );
                return context("inner base for read_array_subtype", fail)(next);
            }
        };
        let nbr_els = match inner_base.el.get_number_elements() {
            Ok(i) => i,
            _ => return context("GhwType::Array: could not determine number of bases", fail)(next),
        };
        let mut next = next;
        let mut rngs = Vec::with_capacity(inner_base.dims.len());
        let mut nbr_scalars = 1i32;
        for _idx in 0..inner_base.dims.len() {
            let rng = GhwRange::parse(next, endian)?;
            next = rng.0;
            rngs.push(Rc::new(rng.1));
            nbr_scalars *= match rngs.last().unwrap().get_length() {
                Ok(n) => n as i32,
                Err(_) => {
                    return context("incompatible range", fail)(next);
                }
            };
        }
        let el: Rc<Self> = if nbr_els >= 0 {
            // element type is bounded
            inner_base.el.clone()
        } else {
            let (n, sa) = Self::read_type_bounds(next, base.clone(), strs, parsed_types, endian)?;
            next = n;
            match &sa {
                GhwType::Array(arr) => {
                    //nbr_els = sa.get_number_elements().expect("will never happen, as array always yields an result for get_number_elements()");
                    arr.el.clone()
                }
                _ => {
                    error!("expected an array as base type, but got {:?}", base);
                    return context("expected an array as base type", fail)(next);
                }
            }
        };

        IResult::Ok((
            next,
            Self::SubArray(GhwSubtypeArray {
                kind: GhdlRtik::SubtypeArray,
                name: Rc::new(String::from("")),
                base,
                nbr_scalars,
                ranges: rngs,
                el,
            }),
        ))
    }

    fn read_record_subtype<'a, E>(
        next_type_data: &'a [u8],
        base: Rc<GhwType>,
        strs: Rc<Vec<Rc<String>>>,
        parsed_types: &Vec<Rc<GhwType>>,
        endian: Endianness,
    ) -> IResult<&'a [u8], GhwSubtypeRecord, E>
    where
        E: ParseError<&'a [u8]>
            + ContextError<&'a [u8]>
            + std::fmt::Debug
            + FromExternalError<&'a [u8], GhwNumError>,
    {
        let mut next = next_type_data;
        let inner_base = match base.deref() {
            Self::Record(r) => r,
            _ => {
                error!("{} is an invalid base type for subtype record", base.name());
                return context("invalid base type", fail)(next);
            }
        };
        trace!(
            "base ({}): nbr_scalars={}, nbr_fields={}",
            inner_base.name,
            inner_base.nbr_scalars,
            inner_base.els.len()
        );
        trace!("inner base: {:?}", inner_base);
        if inner_base.nbr_scalars >= 0 {
            IResult::Ok((
                next,
                GhwSubtypeRecord {
                    kind: GhdlRtik::SubtypeRecord,
                    name: Rc::new(String::new()),
                    nbr_scalars: inner_base.nbr_scalars,
                    els: inner_base.els.clone(),
                    base,
                },
            ))
        } else {
            let mut new_els = Vec::new();
            let mut nbr_scalars = 0;
            for idx in 0..inner_base.els.len() {
                let btype = inner_base.els[idx].ghw_type.clone();
                trace!("btype: {:?}", btype);
                let mut el_nbr_scalars = btype.get_number_elements().unwrap();
                if el_nbr_scalars >= 0 {
                    new_els.push(Rc::new(GhwRecordElement {
                        name: Rc::new(String::new()),
                        ghw_type: btype,
                    }));
                } else {
                    let (n, nt) =
                        Self::read_type_bounds(next, btype, strs.clone(), parsed_types, endian)?;
                    next = n;
                    el_nbr_scalars = nt.get_number_elements().unwrap();
                    new_els.push(Rc::new(GhwRecordElement {
                        name: Rc::new(String::new()),
                        ghw_type: Rc::new(nt),
                    }));
                }
                nbr_scalars += el_nbr_scalars;
            }
            IResult::Ok((
                next,
                GhwSubtypeRecord {
                    kind: GhdlRtik::SubtypeRecord,
                    name: Rc::new(String::new()),
                    base,
                    nbr_scalars,
                    els: new_els,
                },
            ))
        }
    }

    fn read_type_bounds<'a, E>(
        next_type_data: &'a [u8],
        base: Rc<GhwType>,
        strs: Rc<Vec<Rc<String>>>,
        parsed_types: &Vec<Rc<GhwType>>,
        endian: Endianness,
    ) -> IResult<&'a [u8], Self, E>
    where
        E: ParseError<&'a [u8]>
            + ContextError<&'a [u8]>
            + std::fmt::Debug
            + FromExternalError<&'a [u8], GhwNumError>,
    {
        match base.rti_kind() {
            GhdlRtik::TypeArray | GhdlRtik::SubtypeUnboundedArray => {
                trace!("reading type bounds for unbounded array");
                Self::read_array_subtype(next_type_data, base, strs, parsed_types, endian)
            }
            GhdlRtik::TypeRecord | GhdlRtik::SubtypeUnboundedRecord => {
                trace!("reading type bounds for unbounded record");
                let (next, sr) = Self::read_record_subtype(next_type_data, base, strs, parsed_types, endian)?;
                IResult::Ok((next, Self::SubRecord(sr)))
            }
            _ => todo!("emit error"),
        }
    }
    pub fn name(&self) -> Rc<String> {
        match self {
            GhwType::Common(el) => el.name.clone(),
            GhwType::Enum(el) => el.name.clone(),
            GhwType::Scalar(el) => el.name.clone(),
            GhwType::Physical(el) => el.name.clone(),
            GhwType::SubScalar(el) => el.name.clone(),
            GhwType::Array(el) => el.name.clone(),
            GhwType::Record(el) => el.name.clone(),
            GhwType::SubArray(el) => el.name.clone(),
            GhwType::SubUnboundedArray(el) => el.name.clone(),
            GhwType::SubRecord(el) => el.name.clone(),
            GhwType::SubUnboundedRecord(el) => el.name.clone(),
        }
    }
    pub fn base_type(item: &Rc<GhwType>) -> Option<Rc<GhwType>> {
        match item.deref() {
            Self::Enum(_) => Some(item.clone()),
            Self::Physical(_) => Some(item.clone()),
            Self::Array(_) => Some(item.clone()),
            Self::SubArray(sa) => Some(sa.base.clone()),
            Self::SubUnboundedArray(sua) => Some(sua.base.clone()),
            Self::SubRecord(sr) => Some(sr.base.clone()),
            Self::SubUnboundedRecord(sur) => Some(sur.base.clone()),
            Self::SubScalar(ss) => Some(ss.base.clone()),
            _ => {
                error!("no base type for: {:?}", item);
                None
            }
        }
    }
}

impl RtiKind for GhwType {
    fn rti_kind(&self) -> crate::common::GhdlRtik {
        match self {
            GhwType::Common(c) => c.kind,
            GhwType::Enum(e) => e.kind,
            GhwType::Scalar(s) => s.kind,
            GhwType::Physical(p) => p.kind,
            GhwType::SubScalar(ss) => ss.kind,
            GhwType::Array(a) => a.kind,
            GhwType::Record(r) => r.kind,
            GhwType::SubArray(sa) => sa.kind,
            GhwType::SubUnboundedArray(sua) => sua.kind,
            GhwType::SubRecord(sr) => sr.kind,
            GhwType::SubUnboundedRecord(sur) => sur.kind,
        }
    }
}

impl<'a> GhwRange {
    pub fn parse<
        E: ParseError<&'a [u8]>
            + std::fmt::Debug
            + ContextError<&'a [u8]>
            + nom::error::FromExternalError<&'a [u8], GhwNumError>,
    >(
        i: &'a [u8],
        endian: Endianness,
    ) -> IResult<&[u8], Self, E> {
        let dir_kind = context("ghdl direction and kind for range", le_u8)(i)?;
        let mut dir = GhwDir::To;
        if dir_kind.1 & 0x80 == 0x80 {
            dir = GhwDir::Downto;
        };
        match GhdlRtik::from_u8(dir_kind.1 & 0x7f) {
            Some(dir_k) => match dir_k {
                GhdlRtik::TypeB2 => {
                    let (next, (left, right)) = context(
                        "range B2",
                        pair(context("left", le_u8), context("right", le_u8)),
                    )(dir_kind.0)?;
                    IResult::Ok((
                        next,
                        Self::B2(GhwRangeType {
                            kind: dir_k,
                            dir,
                            left,
                            right,
                        }),
                    ))
                }
                GhdlRtik::TypeE8 => {
                    let (next, (left, right)) = context(
                        "range E8",
                        pair(context("left", le_u8), context("right", le_u8)),
                    )(dir_kind.0)?;
                    IResult::Ok((
                        next,
                        Self::E8(GhwRangeType {
                            kind: dir_k,
                            dir,
                            left,
                            right,
                        }),
                    ))
                }
                GhdlRtik::TypeI32 | GhdlRtik::TypeP32 => {
                    let (next, (left, right)) = context(
                        "range I32/P32",
                        pair(
                            context("left", sleb128_to_i32),
                            context("right", sleb128_to_i32),
                        ),
                    )(dir_kind.0)?;
                    IResult::Ok((
                        next,
                        Self::I32(GhwRangeType {
                            kind: dir_k,
                            dir,
                            left,
                            right,
                        }),
                    ))
                }
                GhdlRtik::TypeI64 | GhdlRtik::TypeP64 => {
                    let (next, (left, right)) = context(
                        "range I64/P64",
                        pair(
                            context("left", lsleb128_to_i64),
                            context("right", lsleb128_to_i64),
                        ),
                    )(dir_kind.0)?;
                    IResult::Ok((
                        next,
                        Self::I64(GhwRangeType {
                            kind: dir_k,
                            dir,
                            left,
                            right,
                        }),
                    ))
                }
                GhdlRtik::TypeF64 => {
                    let (next, (left, right)) = context(
                        "range F64",
                        pair(context("left", f64(endian)), context("right", f64(endian))),
                    )(dir_kind.0)?;
                    IResult::Ok((
                        next,
                        Self::F64(GhwRangeType {
                            kind: dir_k,
                            dir,
                            left,
                            right,
                        }),
                    ))
                }
                _ => context("unexpected ghdlrtik for range", fail)(dir_kind.0),
            },
            None => context("unknown ghdlrtik", fail)(dir_kind.0),
        }
    }

    /// TODO: change error kind
    pub fn get_length(&self) -> Result<i64, GhwNumError> {
        let res = match self {
            GhwRange::B2(r) => match r.dir {
                GhwDir::To => r.right as i64 - r.left as i64 + 1,
                GhwDir::Downto => r.left as i64 - r.right as i64 + 1,
            },
            GhwRange::E8(r) => match r.dir {
                GhwDir::To => r.right as i64 - r.left as i64 + 1,
                GhwDir::Downto => r.left as i64 - r.right as i64 + 1,
            },
            GhwRange::I32(r) => match r.dir {
                GhwDir::To => r.right as i64 - r.left as i64 + 1,
                GhwDir::Downto => r.left as i64 - r.right as i64 + 1,
            },
            GhwRange::I64(r) => match r.dir {
                GhwDir::To => r.right - r.left + 1,
                GhwDir::Downto => r.left - r.right + 1,
            },
            GhwRange::F64(_) => return Err(GhwNumError::LSLEB128),
        };
        if res < 0 {
            Ok(0)
        } else {
            Ok(res)
        }
    }
}

impl RtiKind for GhwRange {
    fn rti_kind(&self) -> GhdlRtik {
        match self {
            GhwRange::B2(i) => i.kind,
            GhwRange::E8(i) => i.kind,
            GhwRange::I32(i) => i.kind,
            GhwRange::I64(i) => i.kind,
            GhwRange::F64(i) => i.kind,
        }
    }
}

#[doc(hidden)]
#[cfg(test)]
mod tests {
    use nom::{number::Endianness, IResult};

    use crate::sections::{ghwstrings::Strings, ghwtypes::GhwTypes};

    const TYPE_STRINGS_RAW: &[u8] = &[
        0x53, 0x54, 0x52, 0x00, 0x00, 0x00, 0x00, 0x00, 0x2f, 0x00, 0x00, 0x00, 0x34, 0x01, 0x00,
        0x00, 0x27, 0x2d, 0x27, 0x01, 0x30, 0x27, 0x01, 0x31, 0x27, 0x01, 0x48, 0x27, 0x01, 0x4c,
        0x27, 0x01, 0x55, 0x27, 0x01, 0x57, 0x27, 0x01, 0x58, 0x27, 0x01, 0x5a, 0x27, 0x00, 0x50,
        0x30, 0x00, 0x61, 0x00, 0x62, 0x01, 0x73, 0x01, 0x75, 0x73, 0x5f, 0x73, 0x74, 0x61, 0x74,
        0x65, 0x01, 0x79, 0x74, 0x65, 0x00, 0x63, 0x6c, 0x6b, 0x00, 0x64, 0x61, 0x74, 0x61, 0x00,
        0x65, 0x72, 0x72, 0x6f, 0x72, 0x5f, 0x70, 0x72, 0x6f, 0x62, 0x00, 0x69, 0x6e, 0x74, 0x65,
        0x67, 0x65, 0x72, 0x00, 0x6d, 0x5f, 0x62, 0x75, 0x73, 0x01, 0x61, 0x69, 0x6e, 0x5f, 0x62,
        0x75, 0x73, 0x01, 0x65, 0x6d, 0x03, 0x6f, 0x72, 0x79, 0x00, 0x6e, 0x61, 0x74, 0x75, 0x72,
        0x61, 0x6c, 0x01, 0x75, 0x6d, 0x5f, 0x69, 0x6e, 0x74, 0x04, 0x70, 0x6f, 0x73, 0x04, 0x73,
        0x6d, 0x61, 0x6c, 0x6c, 0x5f, 0x69, 0x6e, 0x74, 0x03, 0x65, 0x72, 0x69, 0x63, 0x5f, 0x73,
        0x74, 0x64, 0x00, 0x6f, 0x68, 0x6d, 0x00, 0x70, 0x6f, 0x73, 0x69, 0x74, 0x69, 0x76, 0x65,
        0x01, 0x72, 0x6f, 0x62, 0x61, 0x62, 0x69, 0x6c, 0x69, 0x74, 0x79, 0x03, 0x67, 0x5f, 0x73,
        0x74, 0x61, 0x74, 0x65, 0x04, 0x72, 0x65, 0x73, 0x73, 0x5f, 0x73, 0x74, 0x61, 0x74, 0x65,
        0x00, 0x72, 0x65, 0x73, 0x69, 0x73, 0x74, 0x61, 0x6e, 0x63, 0x65, 0x00, 0x73, 0x5f, 0x61,
        0x63, 0x74, 0x69, 0x6f, 0x6e, 0x02, 0x69, 0x64, 0x6c, 0x65, 0x03, 0x6e, 0x69, 0x74, 0x01,
        0x68, 0x75, 0x6e, 0x74, 0x01, 0x6d, 0x61, 0x6c, 0x6c, 0x5f, 0x69, 0x6e, 0x74, 0x01, 0x74,
        0x61, 0x6e, 0x64, 0x61, 0x72, 0x64, 0x03, 0x74, 0x65, 0x5f, 0x63, 0x6f, 0x75, 0x6e, 0x74,
        0x73, 0x02, 0x64, 0x5f, 0x6c, 0x6f, 0x67, 0x69, 0x63, 0x09, 0x5f, 0x31, 0x31, 0x36, 0x34,
        0x0a, 0x76, 0x65, 0x63, 0x74, 0x6f, 0x72, 0x04, 0x75, 0x6c, 0x6f, 0x67, 0x69, 0x63, 0x00,
        0x74, 0x62, 0x5f, 0x73, 0x69, 0x67, 0x6e, 0x61, 0x6c, 0x73, 0x01, 0x63, 0x00, 0x45, 0x4f,
        0x53, 0x00,
    ];
    const TYPE_DATA: &[u8] = &[
        0x54, 0x59, 0x50, 0x00, 0x00, 0x00, 0x00, 0x00, 0x16, 0x00, 0x00, 0x00, 0x19, 0x13, 0x22,
        0x13, 0x01, 0x19, 0x80, 0x80, 0x80, 0x80, 0x78, 0xff, 0xff, 0xff, 0xff, 0x07, 0x22, 0x1e,
        0x01, 0x19, 0x01, 0xff, 0xff, 0xff, 0xff, 0x07, 0x22, 0x27, 0x01, 0x19, 0x80, 0x7f, 0xff,
        0x00, 0x1b, 0x1f, 0x22, 0x1f, 0x05, 0x1b, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xf0, 0x3f, 0x1c, 0x22, 0x01, 0x1d, 0x01, 0x22, 0x22,
        0x07, 0x1c, 0x00, 0x80, 0x94, 0xeb, 0xdc, 0x03, 0x17, 0x2d, 0x09, 0x06, 0x08, 0x02, 0x03,
        0x09, 0x07, 0x05, 0x04, 0x01, 0x22, 0x2a, 0x09, 0x17, 0x00, 0x08, 0x17, 0x20, 0x03, 0x25,
        0x24, 0x23, 0x22, 0x18, 0x01, 0x19, 0x00, 0xff, 0xff, 0xff, 0xff, 0x07, 0x1f, 0x2c, 0x0a,
        0x01, 0x0c, 0x23, 0x00, 0x0d, 0x99, 0x01, 0x00, 0x1f, 0x0f, 0x0a, 0x01, 0x02, 0x23, 0x0f,
        0x0f, 0x99, 0x07, 0x00, 0x1f, 0x29, 0x0c, 0x01, 0x0b, 0x23, 0x29, 0x11, 0x17, 0x00, 0x02,
        0x1f, 0x17, 0x10, 0x01, 0x02, 0x23, 0x17, 0x13, 0x99, 0x03, 0x00, 0x23, 0x00, 0x0d, 0x99,
        0x07, 0x00, 0x20, 0x15, 0x03, 0x10, 0x0a, 0x0e, 0x0b, 0x11, 0x15, 0x00, 0x57, 0x4b, 0x54,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x09, 0x00,
    ];
    #[test]
    fn verify_1() {
        let strs: IResult<_, _, nom::error::VerboseError<_>> = Strings::parse(TYPE_STRINGS_RAW);
        let strs = strs.unwrap().1;
        println!("strings: {:#?}", strs);
        println!("============================");
        let types: IResult<_, _, nom::error::VerboseError<_>> =
            GhwTypes::parse(TYPE_DATA, strs.strs, Endianness::Little);
        println!("============================");
        println!("types: {:?}", types);
        //assert!(false, "not implemented yet");
    }
}
