use nom::bytes::complete::take;
use nom::combinator::fail;
use nom::multi::length_count;
use nom::number::complete::u32;
use nom::sequence::delimited;
use nom::{
    bytes::complete::tag,
    combinator::map_res,
    error::{context, ContextError, FromExternalError, ParseError},
    number::{complete::le_u8, Endianness},
    sequence::{preceded, tuple},
    IResult,
};
use tracing::{error, warn};

use crate::{common::ghwendianness, error::GhwNumError};

#[derive(Debug)]
pub struct Directory {
    pub strings: DirectoryEntry,
    pub types: DirectoryEntry,
    pub hierarchy: DirectoryEntry,
    pub snapshot: DirectoryEntry,
    pub cycles: DirectoryEntry,
}

#[derive(Debug)]
pub struct Tail {
    pub endianness: Endianness,
    pub word_size: u8,
    pub file_offset: u8,
    pub dir_pos: u32,
}

#[derive(Debug, Clone, Copy)]
pub struct DirectoryEntry {
    pub pos: u32,
    pub len: u32,
    /// wkt for types and eoh for hierarchy, else None
    /// contains start end len of extention
    pub extention: Option<(u32, u32)>,
}

impl Directory {
    pub fn parse<'a, E>(i: &'a [u8]) -> IResult<&'a [u8], Self, E>
    where
        E: ParseError<&'a [u8]> + ContextError<&'a [u8]> + FromExternalError<&'a [u8], GhwNumError>,
    {
        let (next, (endianness, word_size, file_offset)) = context(
            "directory header",
            delimited(
                tag(b"DIR\0"),
                tuple((
                    context("file endianness", map_res(le_u8, ghwendianness)),
                    context("word size", le_u8),
                    context("file offset", le_u8),
                )),
                tag([0]),
            ),
        )(i)?;
        match endianness {
            Endianness::Big => warn!("big endian not tested"),
            Endianness::Little => {}
            Endianness::Native => return context("unknown endianness", fail)(next),
        }
        if word_size != 4 {
            warn!("WARN: untested word size");
        }
        if file_offset != 1 {
            warn!("WARN: untested file offset")
        }
        let (next, raw_section_info) =
            length_count(u32(endianness), tuple((take(4usize), u32(endianness))))(next)?;
        let mut strings = None;
        let mut types = None;
        let mut hierarchy = None;
        let mut snapshot = None;
        let mut cycles = None;
        for idx in 0..raw_section_info.len() {
            let end_pos = if idx < raw_section_info.len() - 1 {
                raw_section_info[idx + 1].1
            } else if raw_section_info[idx].0 != b"DIR\0" {
                todo!("set start pos of dir");
            } else {
                break;
            };
            let len = end_pos - raw_section_info[idx].1;
            match raw_section_info[idx].0 {
                b"STR\0" => match strings {
                    Some(_) => {
                        return context("DIR: section already exists: str", fail)(next);
                    }
                    None => {
                        strings = Some(DirectoryEntry {
                            pos: raw_section_info[idx].1,
                            len,
                            extention: None,
                        })
                    }
                },
                b"TYP\0" => match types {
                    Some(_) => {
                        return context("DIR: section already exists: typ", fail)(next);
                    }
                    None => {
                        types = Some(DirectoryEntry {
                            pos: raw_section_info[idx].1,
                            len,
                            extention: None,
                        })
                    }
                },
                b"WKT\0" => match types {
                    Some(t) => match t.extention {
                        None => {
                            types = Some(DirectoryEntry {
                                pos: types.unwrap().pos,
                                len: types.unwrap().len,
                                extention: Some((raw_section_info[idx].1, len)),
                            });
                        }
                        _ => return context("Directory: wkt already exists", fail)(next),
                    },
                    None => return context("Directory: wkt missing dir", fail)(next),
                },
                b"HIE\0" => match hierarchy {
                    Some(_) => {
                        return context("DIR: section already exists: hie", fail)(next);
                    }
                    None => {
                        hierarchy = Some(DirectoryEntry {
                            pos: raw_section_info[idx].1,
                            len,
                            extention: None,
                        })
                    }
                },
                b"EOH\0" => match hierarchy {
                    Some(e) => match e.extention {
                        None => {
                            hierarchy = Some(DirectoryEntry {
                                pos: hierarchy.unwrap().pos,
                                len: hierarchy.unwrap().len,
                                extention: Some((raw_section_info[idx].1, len)),
                            });
                        }
                        _ => return context("Directory: eoh already exists", fail)(next),
                    },
                    None => return context("Directory: eoh missing hie", fail)(next),
                },
                b"SNP\0" => match snapshot {
                    Some(_) => {
                        return context("DIR: section already exists: snapshot", fail)(next);
                    }
                    None => {
                        snapshot = Some(DirectoryEntry {
                            pos: raw_section_info[idx].1,
                            len,
                            extention: None,
                        })
                    }
                },
                b"CYC\0" => match cycles {
                    Some(_) => {
                        return context("DIR: section already exists: cycles", fail)(next);
                    }
                    None => {
                        cycles = Some(DirectoryEntry {
                            pos: raw_section_info[idx].1,
                            len,
                            extention: None,
                        })
                    }
                },
                b"DIR\0" => {}
                b"TAI\0" => {}
                _ => {
                    error!("error: {:?}", raw_section_info[idx].0);
                    return context("Directory: unknown type", fail)(next);
                }
            };
        }
        let strings = match strings {
            Some(s) => s,
            _ => return context("Directory: strings missing", fail)(next),
        };
        let types = match types {
            Some(s) => {
                match s.extention {
                    Some(_) => {}
                    None => {
                        return context("Directory: types extention missing", fail)(next);
                    }
                }
                s
            }
            _ => return context("Directory: types missing", fail)(next),
        };
        let hierarchy = match hierarchy {
            Some(s) => {
                match s.extention {
                    Some(_) => {}
                    None => {
                        return context("Directory: hierarchy extention missing", fail)(next);
                    }
                }
                s
            }
            _ => return context("Directory: strings missing", fail)(next),
        };
        let snapshot = match snapshot {
            Some(s) => s,
            _ => return context("Directory: snapshot missing", fail)(next),
        };
        let cycles = match cycles {
            Some(s) => s,
            _ => return context("Directory: cycles missing", fail)(next),
        };
        IResult::Ok((
            next,
            Self {
                strings,
                types,
                hierarchy,
                snapshot,
                cycles,
            },
        ))
    }
}

impl Tail {
    pub fn parse<'a, E>(i: &'a [u8]) -> IResult<&'a [u8], Self, E>
    where
        E: ParseError<&'a [u8]> + ContextError<&'a [u8]> + FromExternalError<&'a [u8], GhwNumError>,
    {
        let (next, (endianness, word_size, file_offset, _)) = preceded(
            tag(b"TAI\0"),
            tuple((
                context("file endianness", map_res(le_u8, ghwendianness)),
                le_u8,
                context("word size", le_u8),
                context("file offset", le_u8),
            )),
        )(i)?;
        let (next, dir_pos) = u32(endianness)(next)?;
        match endianness {
            Endianness::Big => warn!("big endian not tested"),
            Endianness::Little => {}
            Endianness::Native => return context("unknown endianness", fail)(next),
        }
        if word_size != 4 {
            warn!("WARN: untested word size");
        }
        if file_offset != 1 {
            warn!("WARN: untested file offset")
        }
        IResult::Ok((
            next,
            Self {
                endianness,
                word_size,
                file_offset,
                dir_pos,
            },
        ))
    }
}

#[doc(hidden)]
#[cfg(test)]
mod tests {
    use std::io::{prelude::*, SeekFrom};
    use std::{fs::File, io::Seek, path::Path};

    fn get_dir_data(p: &Path, pos: u64) -> Vec<u8> {
        let mut data = Vec::new();
        let mut f = File::open(p).unwrap();
        // let meta = metadata(p).unwrap();
        f.seek(SeekFrom::Start(pos)).unwrap();
        f.read_to_end(&mut data).unwrap();
        data.truncate(data.len() - 12);
        data
    }

    fn get_tail_data(p: &Path) -> Vec<u8> {
        let mut data = Vec::new();
        let mut f = File::open(p).unwrap();
        // let meta = metadata(p).unwrap();
        f.seek(SeekFrom::End(-12)).unwrap();
        f.read_to_end(&mut data).unwrap();
        data
    }
    mod directory {
        use std::path::Path;

        use nom::IResult;

        use crate::sections::ghwdirectory::{Directory, Tail};

        use super::{get_dir_data, get_tail_data};

        #[test]
        fn simple() {
            let path = &Path::new("tests/input/simple.ghw");
            let data = get_tail_data(path);
            let tail: IResult<_, _, nom::error::VerboseError<_>> = Tail::parse(data.as_ref());
            let tail = tail.unwrap().1;
            let data = get_dir_data(path, tail.dir_pos as u64);
            let dir: IResult<_, _, nom::error::VerboseError<_>> = Directory::parse(data.as_ref());
            let dir = dir.unwrap().1;
            println!("dir: {:?}", dir);
            assert_eq!(dir.strings.pos, 16, "strings position");
            assert_eq!(dir.strings.len, 129, "strings length");
            assert_eq!(dir.strings.extention, None, "strings extention");
            assert_eq!(dir.types.pos, 145, "types position");
            assert_eq!(dir.types.len, 31, "types length");
            assert_eq!(dir.types.extention, Some((176, 11)), "types extention");
            assert_eq!(dir.hierarchy.pos, 187, "hierarchy position");
            assert_eq!(dir.hierarchy.len, 58, "hierarchy length");
            assert_eq!(
                dir.hierarchy.extention,
                Some((245, 4)),
                "hierarchy extention"
            );
            assert_eq!(dir.snapshot.pos, 249, "snapshot position");
            assert_eq!(dir.snapshot.len, 25, "snapshot length");
            assert_eq!(dir.snapshot.extention, None, "snapshot extention");
            assert_eq!(dir.cycles.pos, 274, "cycles position");
            assert_eq!(dir.cycles.len, 333, "cycles length");
            assert_eq!(dir.cycles.extention, None, "cycles extention");
        }
    }
    mod tail {
        use std::path::Path;

        use nom::{number::Endianness, IResult};

        use crate::sections::ghwdirectory::Tail;

        use super::get_tail_data;

        #[test]
        fn simple() {
            let data = get_tail_data(&Path::new("tests/input/simple.ghw"));
            let tail: IResult<_, _, nom::error::VerboseError<_>> = Tail::parse(data.as_ref());
            let tail = tail.unwrap().1;
            println!("tail: {:?}", tail);
            assert_eq!(tail.dir_pos, 607, "directory position");
            assert_eq!(tail.endianness, Endianness::Little, "endianness");
            assert_eq!(tail.file_offset, 1, "file offset");
            assert_eq!(tail.word_size, 4, "word size");
        }
        #[test]
        fn signals() {
            let data = get_tail_data(&Path::new("tests/input/signals.ghw"));
            let tail: IResult<_, _, nom::error::VerboseError<_>> = Tail::parse(data.as_ref());
            let tail = tail.unwrap().1;
            println!("tail: {:?}", tail);
            assert_eq!(tail.dir_pos, 813, "directory position");
            assert_eq!(tail.endianness, Endianness::Little, "endianness");
            assert_eq!(tail.file_offset, 1, "file offset");
            assert_eq!(tail.word_size, 4, "word size");
        }
        #[test]
        fn rv32design() {
            let data = get_tail_data(&Path::new("tests/input/rv32design.ghw"));
            let tail: IResult<_, _, nom::error::VerboseError<_>> = Tail::parse(data.as_ref());
            let tail = tail.unwrap().1;
            println!("tail: {:?}", tail);
            assert_eq!(tail.dir_pos, 155361, "directory position");
            assert_eq!(tail.endianness, Endianness::Little, "endianness");
            assert_eq!(tail.file_offset, 1, "file offset");
            assert_eq!(tail.word_size, 4, "word size");
        }
    }
}
