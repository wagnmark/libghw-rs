use std::collections::{BTreeMap, HashMap};

use nom::combinator::{cut, fail, peek, verify};
use nom::multi::{count, length_count, many_till};
use nom::number::complete::{i64, le_u8};
use nom::sequence::delimited;
use nom::{
    bytes::complete::tag,
    error::{context, ContextError, FromExternalError, ParseError},
    number::Endianness,
    sequence::{pair, preceded},
    IResult,
};
use tracing::{debug, error, info, trace};

use crate::common::{lsleb128_to_i64, uleb128_to_u, GhwSig};
use crate::{common::GhwVal, error::GhwNumError};

use super::ghwhierarchy::GhwHierarchy;
use super::ghwsnapshot::Snapshot;

/// all signal values at all time steps
///
/// * all time steps are read on object construction
/// * signal values are read on object construction or dynamically, depending on
/// construction/ parsing function
#[derive(Debug)]
pub struct Cycles {
    pub num_signals: usize,
    pub cycs: Vec<(u64, BTreeMap<u32, GhwVal>)>,
}

impl Cycles {
    /// immedately parse all to memory
    pub fn parse_all<'a, E>(
        i: &'a [u8],
        hie: &mut GhwHierarchy,
        snapshot: Snapshot,
        endian: Endianness,
    ) -> IResult<&'a [u8], Self, E>
    where
        E: ParseError<&'a [u8]>
            + ContextError<&'a [u8]>
            + FromExternalError<&'a [u8], GhwNumError>
            + std::fmt::Debug,
    {
        let signals = if let Some(s) = hie.get_signal_types() {
            s
        } else {
            return context("Cycles: could not get signal list from hierarchy", fail)(i);
        };
        //cycle header (4)
        //  first: current time, after: delta time (lsleb128 from i64), last: -1
        //  changed sig table
        //      idx delta (uleb128 from u32), terminating zero
        //      value
        //TODO: end
        let change_list_parser = |input: &'a [u8]| -> IResult<&'a [u8], _, E> {
            let mut next = input;
            let mut sig_idx = 0;
            let mut sig_vals: BTreeMap<u32, GhwVal> = BTreeMap::new();
            loop {
                let (n, delta_sig_idx) = uleb128_to_u::<u32, E>(next)?;
                next = n;
                if delta_sig_idx == 0 {
                    break;
                }
                sig_idx += delta_sig_idx;
                trace!("\tread signal at index: {}", sig_idx);
                if sig_idx as usize > signals.len() {
                    error!("signal index: {} > {}", sig_idx, signals.len());
                    return context("signal index too big", cut(fail))(next);
                }
                let (n, v) = GhwVal::parse(next, signals[sig_idx as usize - 1].clone(), endian)?;
                next = n;
                trace!("value: {:?}", v);
                sig_vals.insert(sig_idx, v);
            }
            IResult::Ok((next, sig_vals))
        };
        let cycle_parser = |input: &'a [u8]| -> IResult<&'a [u8], _, E> {
            let mut next = input;
            let mut dt;
            let mut vals = BTreeMap::new();
            let mut cycles = Vec::new();
            (next, dt) = i64(endian)(next)?;
            while dt != -1 {
                debug!("read cycle with delta t={}", dt);
                (next, vals) = change_list_parser(next)?;
                cycles.push((dt, vals));
                (next, dt) = lsleb128_to_i64(next)?;
            }
            IResult::Ok((next, cycles))
        };
        let (next, cycs) = context(
            "Cycles",
            delimited(tag(b"CYC\0"), cycle_parser, tag(b"ECY\0")),
        )(i)?;
        debug!("remaining cycle input: {:?}", next);
        info!("cycle count: {:?}", cycs.len());
        let mut time = snapshot.timestamp as u64;
        IResult::Ok((
            next,
            Self {
                num_signals: snapshot.values.len(),
                cycs: cycs
                    .into_iter()
                    .map(|(dt, sig_vals)| {
                        time = time + dt as u64;
                        (time, sig_vals)
                    })
                    .collect(),
            },
        ))
    }
    /// tires get value of signal at index `sig_idx` at time `femto_sec`.
    /// `femto_sec` needs not to match a time step
    ///
    /// only returns none, when `sig_idx` is out of range
    pub fn get_val_time(&self, sig_idx: u32, femto_sec: u64) -> Option<GhwVal> {
        todo!("&cycles.get_val_time(.)");
    }
    /// tries get value of signal at index `sig_idx` at `cyc_idx`
    ///
    /// returns none, when one of the indices is out of range
    pub fn get_val_index(&self, sig_idx: u32, cyc_idx: u64) {
        todo!("&cycles.get_val_index(.)")
    }

    pub fn get_time_steps(&self) -> Vec<TimeStep> {
        todo!("&cycles.get_time_steps(.)")
    }
}

pub struct TimeStep {
    pub index: u64,
    pub femto_sec: u64,
}
