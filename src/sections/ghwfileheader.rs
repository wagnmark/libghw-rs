use nom::{
    bytes::complete::tag,
    combinator::{fail, map, map_res},
    error::{context, ContextError, FromExternalError, ParseError},
    number::{complete::le_u8, Endianness},
    sequence::{pair, preceded, tuple},
    IResult,
};

use crate::{common::ghwendianness, error::GhwNumError};

#[derive(Debug)]
pub struct GhwFileHeader {
    pub header_length: u8,
    pub version: FileVersion,
    pub endian: Endianness,
    pub word_size: u8,
    pub file_offset: u8,
}

#[derive(Debug, Clone, Copy)]
pub struct FileVersion {
    pub major: u8,
    pub minor: u8,
}

impl GhwFileHeader {
    pub fn parse<'a, E>(i: &'a [u8]) -> IResult<&'a [u8], Self, E>
    where
        E: ParseError<&'a [u8]>
            + ContextError<&'a [u8]>
            + std::fmt::Debug
            + FromExternalError<&'a [u8], GhwNumError>,
    {
        let (next, (header_length, version, endian, word_size, file_offset)) = context(
            "file header",
            preceded(
                context("GHW magic header", tag(b"GHDLwave\n")),
                tuple((
                    context("header length", le_u8),
                    context(
                        "file version",
                        map(pair(le_u8, le_u8), |(major, minor)| FileVersion {
                            major,
                            minor,
                        }),
                    ),
                    context("file endianness", map_res(le_u8, ghwendianness)),
                    context("word size", le_u8),
                    context("file offset", le_u8),
                )),
            ),
        )(i)?;
        if header_length != 16 {
            println!("WARN: untested header length");
        }
        if version.major != 0 && version.minor != 1 {
            println!("WARN: wrong file version");
        }
        match endian {
            Endianness::Big => println!("WARN: big endian not tested"),
            Endianness::Little => {}
            Endianness::Native => return context("unknown endianness", fail)(next),
        }
        if word_size != 4 {
            println!("WARN: untested word size");
        }
        if file_offset != 1 {
            println!("WARN: untested file offset")
        }
        IResult::Ok((
            next,
            Self {
                header_length,
                version,
                endian,
                word_size,
                file_offset,
            },
        ))
    }
}

#[doc(hidden)]
#[cfg(test)]
mod tests {
    use nom::{number::Endianness, IResult};

    use super::GhwFileHeader;

    const HEADER: &[u8] = &[
        0x47, 0x48, 0x44, 0x4c, 0x77, 0x61, 0x76, 0x65, 0x0a, 0x10, 0x00, 0x01, 0x01, 0x04, 0x01,
        0x00,
    ];
    #[test]
    fn test_header() {
        let res: IResult<_, _, nom::error::VerboseError<_>> = GhwFileHeader::parse(HEADER);
        let res = res.unwrap().1;
        assert_eq!(res.endian, Endianness::Little, "header endianness");
        assert_eq!(res.file_offset, 1, "header file offset");
        assert_eq!(res.header_length, 16, "header length");
        assert_eq!(res.version.major, 0, "header version major");
        assert_eq!(res.version.major, 0, "header version minor");
        assert_eq!(res.word_size, 4, "header word size");
    }
}
