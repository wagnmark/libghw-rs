library ieee;
use ieee.std_logic_1164.all;

package databus is
    type busmode is (read, write);
    type t_databus is record
        mode: busmode;
        address: std_logic_vector;
        -- ulogic for tristate
        data: std_ulogic_vector;
    end record;
    subtype bus32 is t_databus(address(31 downto 0),data(31 downto 0));
end package;