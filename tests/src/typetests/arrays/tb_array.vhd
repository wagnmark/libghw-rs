library ieee;
use ieee.std_logic_1164.all;

entity tb_array is
end entity;

architecture tb of tb_array is
    type device_state is (init, idle, running, failed);
    type devices_states is array (natural range <>) of device_state;
    signal std_s: std_logic;
    signal std_v: std_logic_vector(3 downto 0);
    signal d_s: device_state;
    signal d_v: devices_states(31 downto 1);
begin
    run: process is
    begin
        wait;
    end process;
end architecture;