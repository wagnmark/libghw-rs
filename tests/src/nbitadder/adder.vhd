library ieee;
use ieee.std_logic_1164.all;

entity adder is
    generic(
        width: positive
    );
    port(
        a, b    : in    std_logic_vector(width - 1 downto 0);
        c_in    : in    std_logic;
        s       : out   std_logic_vector(width - 1 downto 0);
        c_out   : out   std_logic
    );
end entity adder;


architecture structural of adder is
    signal carries: std_logic_vector(0 to width);

    component fulladder is
        port(
            a, b, c_in  : in    std_logic;
            s, c_out    : out   std_logic
        );
    end component;
begin
    carries(0) <= c_in;
    c_out <= carries(width);
    adder_array: for bit_idx in 0 to width - 1 generate
        add1: component fulladder
            port map(
                a => a(bit_idx),
                b => b(bit_idx),
                s => s(bit_idx),
                c_in => carries(bit_idx),
                c_out => carries(bit_idx + 1)
            );
    end generate;
end architecture structural;