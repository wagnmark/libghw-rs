library ieee;
use ieee.std_logic_1164.all;

entity fulladder is
    port(
        a, b, c_in  : in    std_logic;
        s, c_out    : out   std_logic
    );
end entity fulladder;

architecture structural of fulladder is
    signal s_int: std_logic;
    -- complicated, but useful for parser testing
    signal internal_carries: std_logic_vector(1 downto 0);
begin
    main_adder: entity work.halfadder port map(
        a => a,
        b => b,
        s => s_int,
        c => internal_carries(0)
    );
    aux_adder: entity work.halfadder port map(
        a => s_int,
        b => c_in,
        s => s,
        c => internal_carries(1)
    );
    c_out <= internal_carries(0) or internal_carries(1) after 2.1 ns;
end architecture;
