library ieee;
use ieee.std_logic_1164.all;

entity halfadder is
    port(
        a, b    : in    std_logic;
        s, c    : out   std_logic
    );
end entity halfadder;

architecture rtl of halfadder is
begin
    s <= a xor b after 1.2 ns;
    c <= a and b after 1.3 ns;
end architecture;