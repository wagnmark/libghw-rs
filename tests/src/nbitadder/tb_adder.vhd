library ieee;
use ieee.std_logic_1164.all;

entity tb_adder is
end entity;

architecture tb of tb_adder is
    signal stim_a, stim_b, sum: std_logic_vector(4 downto 0);
    signal c_in, c_out: std_logic;
begin
    dut: entity work.adder generic map(
        width => 5
    )
    port map(
        a => stim_a,
        b => stim_b,
        s => sum,
        c_in => c_in,
        c_out => c_out
    );
    stim: process is
    begin
        stim_a(2) <= '0';
        c_in <= '0';
        wait for 30 ns;
        stim_a <= B"1_0010";
        stim_b <= B"1_1111";
        wait for 50 ns;
        assert sum = B"1_0001" report "wrong sum" severity error;
        assert c_out = '1' report "wrong carry out" severity error;
        stim_a(4) <= '0';
        wait for 50 ns;
        assert sum = B"0_0001" report "wrong sum" severity error;
        assert c_out = '1' report "wrong carry out" severity error;
        stim_a <= B"1_1111";
        stim_b <= B"0_0000";
        wait for 50 ns;
        assert sum = B"1_1111" report "wrong sum" severity error;
        assert c_out = '0' report "wrong carry out" severity error;
        c_in <= '1';
        wait for 50 ns;
        assert c_out = '1' report "wrong carry out" severity error;
        wait;
    end process;

end architecture;