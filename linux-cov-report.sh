#!/bin/bash
export RUSTFLAGS="-Zinstrument-coverage"
export LLVM_PROFILE_FILE="coverage-%p-%m.profraw"
rm -f converage*
rm -rf converage/html/
cargo +nightly test
grcov . --binary-path ./target/debug/ -s . -t html --branch --ignore-not-existing --keep-only "src/*" --ignore "src/tests.rs" -o coverage/
rm -f coverage*
firefox file://$(pwd)/coverage/html/index.html
